import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';

import { useFonts } from 'expo-font';


import * as firebase from 'firebase';
import apiKeys from './config/keys';

import { rootReducer } from './reducers/rootReducer';
import thunkMiddleware from 'redux-thunk'

const store = createStore(rootReducer, applyMiddleware(thunkMiddleware));
import { LogBox } from 'react-native';
import MainLayout from './MainLayout';

LogBox.ignoreLogs(['Setting a timer']);
const App = () => {
    const [loaded] = useFonts({
        "Roboto-Black": require('./assets/fonts/Roboto-Black.ttf'),
        "Roboto-Bold": require('./assets/fonts/Roboto-Bold.ttf'),
        "Roboto-Regular": require('./assets/fonts/Roboto-Regular.ttf'),
    })

    if (!loaded) {
        return null;
    }
    if (!firebase.apps.length) {
        firebase.initializeApp(apiKeys.firebaseConfig);
    }

    return (
        <Provider store={store}>
            <MainLayout/>
        </Provider>
    )
}

export default App;