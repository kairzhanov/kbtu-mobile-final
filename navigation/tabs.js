import React, { useEffect } from "react";
import {
    Image
} from 'react-native';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Home } from "../screens/";
import { icons, COLORS, SIZES } from "../constants";
import ProfileScreen from "../screens/ProfileScreen";
import { Colors } from "react-native/Libraries/NewAppScreen";
import CatalogScreen from "../screens/CatalogScreen"
import CartScreen from "../screens/CartScreen"
import { useSelector } from 'react-redux';

const Tab = createBottomTabNavigator();

const tabOptions = {
    showLabel: true,
    style: {
        backgroundColor: '#fff'
    },
    labelStyle: {
        fontSize: 10,
        fontWeight: '500'
    },
    activeTintColor: COLORS.black,
    inactiveTintColor: COLORS.gray
}

const Tabs = () => {
  // Handle user state changes
  
    const len = useSelector(state => state.cart.cartReducer.cart.length)
    return (
        <Tab.Navigator
            tabBarOptions={tabOptions}
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused }) => {
                    const tintColor = focused ? COLORS.black : COLORS.gray;

                    switch (route.name) {
                        case "Home":
                            return (
                                <Image
                                    source={icons.home_icon}
                                    resizeMode="contain"
                                    style={{
                                        tintColor: tintColor,
                                        width: 24,
                                        height: 24
                                    }}
                                />
                            )

                        case "Catalog":
                            return (
                                <Image
                                    source={icons.catalog_icon}
                                    resizeMode="contain"
                                    style={{
                                        tintColor: tintColor,
                                        width: 24,
                                        height: 24
                                    }}
                                />
                            )

                        case "Cart":
                            return (
                                <Image
                                    source={icons.cart_icon}
                                    resizeMode="contain"
                                    style={{
                                        tintColor: tintColor,
                                        width: 24,
                                        height: 24
                                    }}
                                />
                            )

                        case "Profile":
                            return (
                                <Image
                                    source={icons.profile_icon}
                                    resizeMode="contain"
                                    style={{
                                        tintColor: tintColor,
                                        width: 24,
                                        height: 24
                                    }}
                                />
                            )
                    }
                }
            })}
        >
            <Tab.Screen
                name="Home"
                component={Home}
                options={{tabBarLabel: "Главная"}}
            />
            <Tab.Screen
                name="Catalog"
                component={CatalogScreen}
                options={{tabBarLabel: "Каталог"}}
            />
            <Tab.Screen
                name="Cart"
                component={CartScreen}
                options={{
                    tabBarBadge: len>0?len:null,
                    tabBarLabel: "Корзина"
                }}

            />
            <Tab.Screen
                name="Profile"
                component={ProfileScreen}
                options={{tabBarLabel: "Профиль"}}
            />
        </Tab.Navigator>
    )
}

export default Tabs;