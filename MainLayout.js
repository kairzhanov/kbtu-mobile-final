import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from '@react-navigation/native';
import * as firebase from 'firebase';

import {getUser} from './actions/userActions'

import { BookDetail } from "./screens/";
import Tabs from "./navigation/tabs";
import ProductSection from './screens/ProductSection'
import SignUp from './screens/SignUp';
import SignIn from './screens/SignIn';
import CheckoutScreen from './screens/CheckoutScreen';
import OrdersScreen from './screens/OrdersScreen'
import PersonalDataScreen from './screens/PersonalDataScreen'
import ChangePasswordScreen from './screens/ChangePasswordScreen'
import SelectCityScreen from './screens/SelectCityScreen';
import FeedbackScreen from './screens/FeedbackScreen';
import OrderDetail from './screens/OrderDetail';
import Filter from './screens/Filter';
import SearchResults from './screens/SearchResults';
import { useDispatch } from 'react-redux';



const Stack = createStackNavigator();

const MainLayout = () => {

    const dispatch = useDispatch()
    const [loading, setLoading] = useState(true)

    useEffect(() => {
      firebase.auth().onAuthStateChanged((user) => {
        if (user) {
          dispatch(getUser())
          setLoading(false)
        }
      });
    }, []) 

    if (loading) {	
      return (	
        <View
            style={{
                flex:1,
                justifyContent:'center',
                alignItems:'center'
            }}
        >
            <Text>ONSHOP</Text>
        </View>	
      )	
    }

    return (
        <NavigationContainer >
            <Stack.Navigator
                screenOptions={{
                    headerShown: false
                }}
                initialRouteName={'Home'}
            >
                {/* Tabs */}
                <Stack.Screen name="Home" component={Tabs} />

                {/* Screens */}
                <Stack.Screen name="BookDetail" component={BookDetail} />
                <Stack.Screen name="ProductSection" component={ProductSection} />

                <Stack.Screen name='Sign Up' component={SignUp} />
                <Stack.Screen name='Sign In' component={SignIn} />
                <Stack.Screen name='Checkout' component={CheckoutScreen} />
                <Stack.Screen name='Orders' component={OrdersScreen} />
                <Stack.Screen name='PersonalData' component={PersonalDataScreen} />
                <Stack.Screen name='ChangePassword' component={ChangePasswordScreen} />
                <Stack.Screen name='SelectCity' component={SelectCityScreen} />
                <Stack.Screen name='Feedback' component={FeedbackScreen} />
                <Stack.Screen name='OrderDetail' component={OrderDetail} />
                <Stack.Screen name='Filter' component={Filter} />
                <Stack.Screen name='SearchResults' component={SearchResults} />


            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default MainLayout

const styles = StyleSheet.create({})
