import { CHANGE_PASSWORD, GET_USER, RE_AUTH, SIGN_IN, SIGN_OUT, SIGN_UP } from "../types/types";
import * as firebase from "firebase";
import "firebase/firestore";
import { Alert } from "react-native";


export const signIn = (email, password) => async dispatch => {
    try {
        await firebase
            .auth()
            .signInWithEmailAndPassword(email, password)
            .then((userInfo) => {
                dispatch({
                    type: SIGN_IN,
                    payload: userInfo
                })
                dispatch(getUser())
            })
    } catch (err) {
        Alert.alert("Ошибка входа!", err.message);
    }
}


export const loggingOut = () => async dispatch => {
    try {
        await firebase.auth().signOut();
        dispatch({
            type: SIGN_OUT
        })
    } catch (err) {
        Alert.alert('Ошибка выхода!', err.message);
    }
}

export const getUser = () => async dispatch => {
    try {
        let doc = await firebase
            .firestore()
            .collection('users')
            .doc(firebase.auth().currentUser.uid)
            .get();

        if (!doc.exists) {
            Alert.alert('No user data found!')
        } else {
            let dataObj = doc.data();
            dispatch({
                type: GET_USER,
                payload: dataObj
            })
        }
    } catch (err) {
        Alert.alert('Ошибка получения данных пользоваталя', err.message)
    }
}

export const registration = (email, password, tel, firstName) => async dispatch => {
    try {
        await firebase.auth().createUserWithEmailAndPassword(email, password)
            .catch((error) => {
            var errorCode = error.code;
            var errorMessage = error.message;
            Alert.alert(errorMessage.toString(), errorCode.toString());
          });;
        const currentUser = firebase.auth().currentUser;

        const db = firebase.firestore();
        db.collection("users")
            .doc(currentUser.uid)
            .set({
                email: currentUser.email,
                tel: tel,
                firstName: firstName,
            });
        dispatch({
            type: SIGN_UP
        })
    } catch (err) {
        Alert.alert("Ошибка при регистрации", err.message);
    }
}

export const changeProfileData = (firstName, tel, email) => async dispatch => {
    try {
        //dispatch(reAuth())
        const user = firebase.auth().currentUser;

        let db = firebase.firestore();
        db.collection("users").doc(user.uid).update(
            {
                firstName: firstName,
                tel: tel,
                email: email
            }
        );

        user.updateEmail(email).then(() => {
            dispatch(getUser())
            console.log('Данные успешно изменены');
            Alert.alert("Данные успешно изменены", 'Данные успешно изменены');
        }).catch((error) => {
            Alert.alert("ошибка при изменении данных", error.toString());
        });
    } catch (err) {
        Alert.alert("Возникла ошибка при изменении данных", err.message);
    }
}

export const changePassword = (newPassword) => async dispatch => {
    try {
        //dispatch(reAuth())
        const user = firebase.auth().currentUser;

        user.updatePassword(newPassword).then(() => {
            dispatch({
                type: CHANGE_PASSWORD
            })
            Alert.alert("Измение пароля", "пароль изменен");
        }).catch((error) => {
            Alert.alert("ошибка при изменении пароля", error.toString());
        });
    } catch (err) {
        Alert.alert("Возникла ошибка при изменении пароля", err.message);
    }
}

export const reAuth = () => async dispatch=>{
    try {
        const user = firebase.auth().currentUser;

        const credential = promptForCredentials();

        user.reauthenticateWithCredential(credential).then(() => {
            dispatch({
                type: RE_AUTH
            })
        }).catch((error) => {
            console.log(error)
            Alert.alert("ошибка перезахода", error.toString());
        });
    } catch (err) {
        Alert.alert("Возникла ошибка при перезаходе", err.message);
    }
}