import { ADD_TO_CART, CHANGE_CITY, CHANGE_CURRENCY, DELETE_FROM_CART, MAKE_ORDER } from '../types/types'

export const addToCart = product => (
  {
    type: ADD_TO_CART,
    product
  }
)

export const makeOrder = (deliveryDetails) => (
  {
    type: MAKE_ORDER,
    payload: deliveryDetails
  }
)

export const deleteFromCart = product => (
  {
    type: DELETE_FROM_CART,
    payload: product
  }
)

export const changeCurrency = currency =>(
  {
    type: CHANGE_CURRENCY,
    payload: currency
  }
)

export const changeCity = city => (
  {
    type: CHANGE_CITY,
    payload: city
  }
)