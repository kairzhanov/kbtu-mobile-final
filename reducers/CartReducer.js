import { combineReducers } from 'redux';
import { MAKE_ORDER, ADD_TO_CART, DELETE_FROM_CART, SET_CURRENCY, CHANGE_CURRENCY, CHANGE_CITY } from '../types/types'
import moment from "moment";

const INITIAL_STATE = {
    cart: [],
    cartPrice: 0,
    orders: [],
    currency: 'KZT',
    city: 
        {
            'id':0,
            'city': 'Алматы',
            'geo': 'Казахстан'
        },
};

const cartReducer = (state = INITIAL_STATE, action) => {
    let isAdded = -1
    switch (action.type) {

        case MAKE_ORDER:
            let x = [...state.orders, ...state.cart]
            x.forEach((item,i) => {
                if (!item.orderNumber) {

                    item.orderNumber = Date.now() + item.id
                    item.currency = state.cart.currency
                    item.city = action.payload.city
                    item.street = action.payload.street
                    item.home = action.payload.home
                    item.deliveryDate = action.payload.deliveryDate
                    item.deliveryPrice = action.payload.deliveryPrice
                    item.paymentMethod = action.payload.payMethod
                    item.firstName = action.payload.firstName
                    item.tel = action.payload.tel
                    item.email = action.payload.email
                    item.comment = action.payload.comment
                    item.orderStatus = 'inprocessing'
                }
            });
            return { 
                ...state, 
                cart: [], 
                orders: x, 
                cartPrice: 0 
            };

        case ADD_TO_CART:

            let addedItem = action.product
            isAdded = state.cart.findIndex(item => addedItem.id === item.id)
            if (isAdded>=0) {
                state.cart[isAdded].qty++
                let u = { ...state, cart: state.cart ,cartPrice: state.cartPrice+addedItem.price}

                return u
            } else {
                let newProduct = action.product
                let z = {
                    ...state,
                    cart: [...state.cart, { ...newProduct, qty: 1 }],
                    cartPrice: state.cartPrice + newProduct.price
                }
                return z
            }

        case DELETE_FROM_CART:

            isAdded = state.cart.indexOf(action.payload)

            if (isAdded >= 0) {
                if (state.cart[isAdded].qty === 1) {
                    state.cart = state.cart.filter(item => item.id !== state.cart[isAdded].id)
                    return { ...state, cart: state.cart, cartPrice: state.cartPrice - action.payload.price }
                }
                state.cart[isAdded].qty--
                return { ...state, cart: state.cart, cartPrice: state.cartPrice - action.payload.price }
            }
        case CHANGE_CURRENCY:
            return { ...state, currency: action.payload}
        case CHANGE_CITY:
            return {...state, city: action.payload}
        default:
            return state
    }
};

export default combineReducers({
    cartReducer: cartReducer
});