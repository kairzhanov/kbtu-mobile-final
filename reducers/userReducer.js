import { combineReducers } from "redux";
import { CHANGE_PASSWORD, CHANGE_PROFILE_DATA, GET_USER, RE_AUTH, SIGN_IN, SIGN_OUT, SIGN_UP } from "../types/types";

const INITIAL_STATE = {
    userData : {},
    profileData: {}
}

const userReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SIGN_IN:
            return {...state, userData: action.payload, profileData: action.payload}
        case SIGN_OUT:
            return {...state, userData: {}, profileData:{}}
        case GET_USER:
            return {...state, profileData: action.payload}
        case SIGN_UP:
            return {...state}
        case CHANGE_PROFILE_DATA:
            return {...state}
        case CHANGE_PASSWORD:
            return {...state}
        case RE_AUTH:
            return {...state}
        default: return state
    }
}


export default combineReducers({
    userReducer: userReducer
})