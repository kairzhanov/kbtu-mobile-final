import { combineReducers } from "redux";
import CartReducer from "./CartReducer";
import userReducer from "./userReducer";

export const rootReducer = combineReducers({
    cart: CartReducer,
    user: userReducer
})