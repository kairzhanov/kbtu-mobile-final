import React, { useState } from 'react';
import { View, Text, TextInput, StyleSheet, Alert, SafeAreaView, ScrollView } from 'react-native';
//import { signIn } from '../API/firebaseMethods';
import {
  Header,
  Button,
  Icon,
  Input
} from "react-native-elements";
import { connect, useDispatch } from 'react-redux';
import { signIn } from '../actions/userActions';

const SignIn =(props) => {
  const [email, setEmail] = useState('ivanivanov3@mail.com');
  const [password, setPassword] = useState('password');
  const [secure, setSecure] = useState(true)
  
  const handlePress = () => {
    if (!email) {
      Alert.alert('Email field is required.');
    }

    if (!password) {
      Alert.alert('Password field is required.');
    }
    props.signIn(email,password)
    props.navigation.navigate('Home');
    setEmail('');
    setPassword('');
  };

  return (
    <SafeAreaView style={{ flex: 1 }} >
      <Header
        leftComponent={
          <Icon
            name='arrow-left'
            type="feather"
            onPress={() => props.navigation.goBack()}
          />
        }
        centerComponent={{
          text: "Вход",
          style: { fontWeight: '600', fontSize: 17 },
        }}
        containerStyle={{
          backgroundColor: "#F5F5F5",
          paddingBottom: 8,
        }}
        statusBarProps={{
          backgroundColor: "#F5F5F5",
          barStyle: "dark-content",
        }}
      />
      <ScrollView style={{ padding: 16 }} >
        <Text style={styles.text}>Номер телефона или E-mail</Text>

        <TextInput
          style={styles.formInput}
          placeholder="Введите номер телефона или E-mail"
          value={email}
          onChangeText={email => setEmail(email)}
          autoCapitalize="none"
          autoCompleteType={'email'}
          keyboardType="email-address"
        />
        <Text style={styles.text}>Пароль</Text>
        <View  style={{
          flex:1,
          flexWrap:'wrap',
          flexDirection:'row'
        }}>

        <TextInput
          style={styles.passwordInput}
          placeholder="Введите пароль"
          value={password}
          onChangeText={(password) => setPassword(password)}
          secureTextEntry={secure}
        />
        {
          secure ? 
          <Icon
            name='eye'
            type="feather"
            color='#BDBDBD'
            containerStyle={styles.secureIcon}
            onPress={()=>setSecure(!secure)}
          /> 
          :
          <Icon
            name='eye-off'
            type="feather"
            color='#BDBDBD'
            containerStyle={styles.secureIcon}
            onPress={()=>setSecure(!secure)}
          />
        }
        </View>
            
      </ScrollView>
      <View>
        <View style={{
          height: 70,
          padding: 10,
        }}>
          <Button 
            buttonStyle={{
              paddingVertical: 15,
              alignSelf: 'stretch',
              borderRadius: 8,
              backgroundColor: '#4D77FB'
            }}
            onPress={handlePress}
            title="Войти" 
            disabled={email && password ? false : true}
            disabledTitleStyle={{
              color:'#fff'
            }}
            disabledStyle={{
              paddingVertical: 15,
              alignSelf: 'stretch',
              borderRadius: 8,
              backgroundColor:'#BDBDBD'
            }}
          />
        </View>
      </View>

    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  button: {
    paddingVertical: 12,
    borderWidth: 2,
    borderRadius: 8,
    alignSelf: 'center',
  },
  buttonText: {
    fontSize: 17,
    textAlign: 'center',
  },
  formInput: {
    flex: 1,
    fontSize: 17,
    padding: 14,
    backgroundColor: '#fff',
    marginBottom: 10,
    marginTop: 4,
    borderRadius: 8
  },
  passwordInput:{
    flex:1,
    padding: 14,
    backgroundColor: '#fff',
    marginBottom: 10,
    marginTop: 4,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8
  },
  text: {
    color: "#9E9E9E",
  },
  secureIcon:{
    padding: 12,
    backgroundColor: '#fff',
    marginBottom: 10,
    marginTop: 4,
    borderBottomRightRadius: 8,
    borderTopRightRadius: 8
  }
});

const mapDispatchToProps ={
  signIn 
}

export default connect(null, mapDispatchToProps)(SignIn)