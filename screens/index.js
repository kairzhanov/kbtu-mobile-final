import Home from "./Home";
import BookDetail from "./BookDetail"
import ProductSection from "./ProductSection"

export {
    Home,
    BookDetail,
    ProductSection
};