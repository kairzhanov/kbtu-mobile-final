import React from "react";
import {
  StyleSheet,
  SafeAreaView,
  View,
  TouchableOpacity,
  Image,
  Text,
} from "react-native";
import { Header, Button, Icon } from "react-native-elements";
import { FlatList } from "react-native";
import { connect, useSelector, useDispatch } from 'react-redux';
import { deleteFromCart, addToCart, makeOrder } from './../actions/CartActions';

function CartScreen(props) {
  const cart = useSelector(state => state.cart.cartReducer)
  const dispatch = useDispatch()
  const countRus = () => {
    if (cart.cart.length % 10 == 1) {
      return 'товар'
    } else if (cart.cart.length % 10 == 2 || cart.cart.length % 10 == 3 || cart.cart.length % 10 == 4) {
      return 'товара'
    } else {
      return 'товаров'
    }
  }
  const numberWithSpaces = x => x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")

  const renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => props.navigation.navigate("BookDetail", {
          book: item
        })}
      >
        <Image
          source={{ uri: item.image }}
          resizeMode="cover"
          style={styles.adsImageCover}
        />
        <View style={{ flex: 1, justifyContent: "space-between", marginLeft: 8 }}>
          <View>
            <Text style={{ fontWeight: '500', fontSize: 13 }}>{item.name}</Text>
            {/* <Text style={{ color: "red" }}>-40% от 99шт.</Text> */}
          </View>
          <View style={{ flexDirection: 'row', justifyContent: "space-between", alignItems: 'flex-end' }}>
            <View style={{}}>
              <Text style={{ fontWeight: '500', fontSize: 10, color: '#9E9E9E' }}>{numberWithSpaces(item.price)} $ х {item.qty}шт.</Text>
              <Text style={{ fontWeight: '600', fontSize: 12 }}>{numberWithSpaces(item.price * item.qty)} $</Text>
            </View>
            <View style={{ backgroundColor: '#F5F5F5', flexDirection: 'row', padding: 7, borderRadius: 4, alignItems: 'center' }}>
              <Icon name='minus' type="feather"
                onPress={() => dispatch(deleteFromCart(item))} />
              <Text style={{ marginHorizontal: 17, fontWeight: '400', fontSize: 15 }}>{item.qty}</Text>
              <Icon name='plus' type="feather" onPress={() => dispatch(addToCart(item))} />
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <SafeAreaView style={styles.droidSafeArea}>
      <Header
        centerComponent={{
          text: "Корзина",
          style: { fontWeight: "600", fontSize: 17 },
        }}
        containerStyle={{
          backgroundColor: "#F5F5F5",
          paddingBottom: 12,
        }}
        statusBarProps={{
          backgroundColor: "#F5F5F5",
          barStyle: "dark-content",
        }}
      />
      <FlatList
        data={cart.cart}
        renderItem={renderItem}
        keyExtractor={(item) => `${item.id}`}
        showsHorizontalScrollIndicator={false}
      />
      {cart.cart.length > 0 && cart.cartPrice > 0 ? (
        <View>
          <View style={styles.checkoutContainer}>
            <View>
              <View>
                <Text style={{ fontSize: 12, fontWeight: '400', marginBottom: 4 }}>{cart.cart.length} {countRus()}</Text>
                <Text style={{ fontSize: 15, fontWeight: '500' }}>{numberWithSpaces(cart.cartPrice)} $</Text>
              </View>
            </View>
            <View>
              <Button
                title="Оформить заказ"
                onPress={() => props.navigation.navigate("Checkout")}
                // onPress={() =>{
                //   dispatch(makeOrder())
                //       props.navigation.navigate("Home")
                //   }}
                buttonStyle={{
                  paddingVertical: 12,
                  width: '100%',
                  backgroundColor: '#4D77FB',
                  borderRadius: 8
                }}
                titleStyle={{
                  fontWeight: '500',
                  fontSize: 17
                }}
              />
            </View>
          </View>
        </View>) :
        <View></View>
      }

    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  droidSafeArea: {
    flex: 1,
    backgroundColor: "#F5F5F5",
  },
  adsImageCover: {
    width: 84,
    height: 84,
    borderRadius: 4,
  },
  container: {
    flex: 1,
    margin: 5,
    backgroundColor: "#fff",
    flexDirection: "row",
    borderRadius: 12,
    marginHorizontal: 16,
    height: 100,
    padding: 8
  },
  checkoutContainer: {
    flexDirection: "row",
    justifyContent: 'space-between',
    height: 60,
    alignItems: 'center',
    paddingHorizontal: 8,
    backgroundColor: '#fff',
  }
});

export default CartScreen