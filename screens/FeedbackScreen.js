import React, {useState, useEffect} from 'react'
import { ScrollView, FlatList, TouchableOpacity, View, Text, StyleSheet, TextInput, Button } from 'react-native'
import { SearchBar, ListItem, Icon, Header } from 'react-native-elements'
import { SafeAreaView } from 'react-native-safe-area-context'


const FeedbackScreen = (props) => {
    const handlePress = () => {

    }
    return (
        <SafeAreaView style={{flex:1}}>
            <Header 
                leftComponent={
                    <Icon 
                        name='arrow-left' 
                        type="feather" 
                        onPress={() => props.navigation.goBack()} 
                    />
                }
                centerComponent={{
                    text: "Обратная связь",
                    style: { fontWeight: "600", fontSize: 17 },
                }}
                rightComponent={
                    <TouchableOpacity>
                        <Text style={{color:'#007AFF', fontSize: 13, fontWeight:'500'}}>Отправить</Text>
                    </TouchableOpacity>
                }
                containerStyle={{
                    backgroundColor: "#F5F5F5",
                    paddingBottom: 12,
                }}
                statusBarProps={{
                    backgroundColor: "#F5F5F5",
                    barStyle: "dark-content",
                }}
            />
             <View style={{ padding: 16 }} >
        <Text style={styles.text}>Представьтесь</Text>
        <TextInput
          style={styles.formInput}
          onChangeText={(name) => setFirstName(name)}
        />
        <Text style={styles.text}>Ваша эл. почта</Text>
        <TextInput
          style={styles.formInput}
          onChangeText={(name) => setTel(name)}
        />

        <Text style={styles.text}>Тема сообщения</Text>

        <TextInput
          style={styles.formInput}
          onChangeText={(email) => setEmail(email)}
          keyboardType="email-address"
          autoCapitalize="none"
        />
        <Text style={styles.text}>Ваше сооббщение</Text>

        <TextInput
          style={styles.formInput}
          onChangeText={(password) => setPassword(password)}
        />

      </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    button: {
      paddingVertical: 12,
      borderWidth: 2,
      borderRadius: 8,
      alignSelf: 'center',
    },
    buttonText: {
      fontSize: 17,
      textAlign: 'center',
    },
    formInput: {
      fontSize: 17,
      padding: 14,
      backgroundColor: '#fff',
      marginBottom: 10,
      marginTop: 4,
      borderRadius: 12
    },
    text: {
      color: "#9E9E9E",
    }
  });
export default FeedbackScreen
