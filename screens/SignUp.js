import React, { useState } from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  SafeAreaView,
  Alert
} from 'react-native';
//import { registration } from '../API/firebaseMethods';
import {
  Header,
  Button,
  Icon
} from "react-native-elements";
import { registration } from '../actions/userActions';



export default function SignUp({ navigation }) {

  const [firstName, setFirstName] = useState('');
  const [tel, setTel] = useState('')
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [secure, setSecure] = useState(true)
  const [secureConfirm, setSecureConfirm] = useState(true)

  const emptyState = () => {
    setFirstName('');
    setTel('');
    setEmail('');
    setPassword('');
    setConfirmPassword('');
  };

  const handlePress = () => {
    if (!firstName) {
      Alert.alert('First name is required');
    } else if (!email) {
      Alert.alert('Email field is required.');
    } else if (!password) {
      Alert.alert('Password field is required.');
    } else if (!confirmPassword) {
      setPassword('');
      Alert.alert('Confirm password field is required.');
    } else if (password !== confirmPassword) {
      Alert.alert('Password does not match!');
    } else {
      registration(
        email,
        password,
        tel,
        firstName,
      );
      navigation.navigate('Home');
      emptyState();
    }
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Header
        leftComponent={
          <Icon
            name='arrow-left'
            type="feather"
            onPress={() => navigation.goBack()}
          />
        }
        centerComponent={{
          text: "Регистрация",
          style: { fontWeight: '600', fontSize: 17 },
        }}
        containerStyle={{
          backgroundColor: "#F5F5F5",
          paddingBottom: 12,
        }}
        statusBarProps={{
          backgroundColor: "#F5F5F5",
          barStyle: "dark-content",
        }}
      />
      <View style={{ padding: 16 }} >
        <Text style={styles.text}>Ваше имя</Text>
        <TextInput
          style={styles.formInput}
          placeholder="Введите Ваше имя"
          value={firstName}
          onChangeText={(name) => setFirstName(name,)}
        />
        <Text style={styles.text}>Номер телефона</Text>
        <TextInput
          style={styles.formInput}
          placeholder="+7 ( --- ) --- -- --"
          value={tel}
          onChangeText={(name) => setTel(name.replace(/[^0-9]/g, ''))}
          keyboardType = 'number-pad'
        />
        <Text style={styles.text}>Почта</Text>

        <TextInput
          style={styles.formInput}
          placeholder="Введите ваш e-mail"
          value={email}
          onChangeText={(email) => setEmail(email)}
          keyboardType="email-address"
          autoCapitalize="none"
        />
        <Text style={styles.text}>Пароль</Text>
        <View style={{
          flexWrap: 'wrap',
          flexDirection: 'row'
        }}>
          <TextInput
            style={styles.passwordInput}
            placeholder="Придумайте пароль"
            value={password}
            onChangeText={(password) => setPassword(password)}
            secureTextEntry={secure}
          />
          {
              secure ?
                <Icon
                  name='eye'
                  type="feather"
                  color='#BDBDBD'
                  containerStyle={styles.secureIcon}
                  onPress={() => setSecure(!secure)}
                />
                :
                <Icon
                  name='eye-off'
                  type="feather"
                  color='#BDBDBD'
                  containerStyle={styles.secureIcon}
                  onPress={() => setSecure(!secure)}
                />
            }
          </View>

        <Text style={styles.text}>Подтвердите пароль</Text>

        <View style={{
          flex: 1,
          flexWrap: 'wrap',
          flexDirection: 'row'
        }}>
          <TextInput
            style={styles.passwordInput}
            placeholder="Подтвердите пароль"
            value={confirmPassword}
            onChangeText={(password2) => setConfirmPassword(password2)}
            secureTextEntry={secureConfirm}
          />
          {
            secureConfirm ?
              <Icon
                name='eye'
                type="feather"
                color='#BDBDBD'
                containerStyle={styles.secureIcon}
                onPress={() => setSecureConfirm(!secureConfirm)}
              />
              :
              <Icon
                name='eye-off'
                type="feather"
                color='#BDBDBD'
                containerStyle={styles.secureIcon}
                onPress={() => setSecureConfirm(!secureConfirm)}
              />
          }
        </View>
      </View>
      <View style={{ position: 'absolute', left: 0, right: 0, bottom: 0 }}>
        <View style={{
          padding: 8,
          backgroundColor: '#FFF'
        }}>
          <Button
            disabled={(firstName && tel && email && password && confirmPassword ? false : true)}
            disabledTitleStyle={{
              color:'#fff'
            }}
            disabledStyle={{
              paddingVertical: 15,
              alignSelf: 'stretch',
              borderRadius: 8,
              backgroundColor:'#BDBDBD'
            }}
            buttonStyle={{
              paddingVertical: 12,
              alignSelf: 'stretch',
              borderRadius: 8,
              backgroundColor: '#4D77FB'
            }}
            onPress={handlePress}
            title="Зарегистрироваться"
            titleStyle={{
              fontSize: 17,
              fontWeight: '500',
              lineHeight: 20.29
            }}
          />
        </View>
      </View>

    </SafeAreaView>
  );
}


const styles = StyleSheet.create({
  button: {
    paddingVertical: 12,
    borderWidth: 2,
    borderRadius: 8,
    alignSelf: 'center',
  },
  buttonText: {
    fontSize: 17,
    textAlign: 'center',
  },
  formInput: {
    fontSize: 17,
    padding: 14,
    backgroundColor: '#fff',
    marginBottom: 10,
    marginTop: 4,
    borderRadius: 8
  },
  text: {
    color: "#9E9E9E",
  },
  passwordInput: {
    flex: 1,
    fontSize: 17,
    padding: 14,
    backgroundColor: '#fff',
    marginBottom: 10,
    marginTop: 4,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8
  },
  secureIcon: {
    padding: 12,
    backgroundColor: '#fff',
    marginBottom: 10,
    marginTop: 4,
    borderBottomRightRadius: 8,
    borderTopRightRadius: 8
  }
});