import React, { useState } from 'react'
import { View, ScrollView, Text, StyleSheet, TextInput, SafeAreaView, KeyboardAvoidingView } from 'react-native'
import { Header, Icon, Button } from 'react-native-elements'
import Delivery from '../components/checkout/Delivery';
import Payment from '../components/checkout/Payment';
import Data from '../components/checkout/Data'
import Check from '../components/checkout/Check';
import { useDispatch, useSelector } from 'react-redux';
import { makeOrder } from '../actions/CartActions';

function CheckoutScreen(props) {

    const order = useSelector(state => state.cart.cartReducer)
    const user = useSelector(state => state.user.userReducer)
    const [city, setCity] = useState('Almaty')
    const [street, setStreet] = useState('Lenina')
    const [home, setHome] = useState('23')
    const [flat, setFlat] = useState('45')
    const [deliveryDate, setDeliveryDate] = useState(Date.now()+(1000 * 60 * 60 * 24 * 7))
    const [deliveryPrice, setDeliveryPrice] = useState(1000)

    const [payMethod, setPayMethod] = useState({name:'CASH', title:'Наличными'})

    const [firstName, setFirstName] = useState(user.profileData.firstName)
    const [tel, setTel] = useState(user.profileData.tel)
    const [email, setEmail] = useState(user.profileData.email)

    const [comment, setComment] = useState('good 😎')

    const dispatch = useDispatch()

    const [step, setStep] = useState(0)

    const handleNextStep = () => {
        if (step === 3) {
            dispatch(makeOrder({
                city, street, home, flat, deliveryDate, deliveryPrice,payMethod, firstName, tel, email, comment
            }))
            props.navigation.navigate("Home")
        } else {
            setStep(step + 1)
        }
    }
    return (
        <KeyboardAvoidingView
            {...(Platform.OS === 'ios' ? { behavior: 'padding' } : {})}
            style={{ flex: 1 }}
        >
            <SafeAreaView style={{ flex: 1 }}>
                <Header
                    leftComponent={
                        <Icon
                            name='arrow-left'
                            type="feather"
                            onPress={() => props.navigation.goBack()}
                        />
                    }
                    centerComponent={{
                        text: 'Оформление заказа',
                        style: { fontWeight: "600", fontSize: 17 },
                    }}
                    containerStyle={{
                        backgroundColor: "#F5F5F5",
                        paddingBottom: 12,
                    }}
                    statusBarProps={{
                        backgroundColor: "#F5F5F5",
                        barStyle: "dark-content",
                    }}
                />
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'flex-start', paddingTop: 8, paddingHorizontal: 16, alignItems: 'stretch' }}>
                    <View>
                        <Icon name={"truck"} type="feather" color={step >= 0 ? '#000' : '#9E9E9E'} />
                        <Text style={{ ...styles.operText, color: step >= 0 ? '#000' : '#9E9E9E' }}>Доставка</Text>
                    </View>

                    <View style={{ ...styles.separator, backgroundColor: step >= 1 ? '#000' : '#9E9E9E' }} />

                    <View>
                        <Icon name={"credit-card"} type="feather" color={step >= 1 ? '#000' : '#9E9E9E'} />
                        <Text style={{ ...styles.operText, color: step >= 1 ? '#000' : '#9E9E9E' }}>Оплата</Text>
                    </View>

                    <View style={{ ...styles.separator, backgroundColor: step >= 2 ? '#000' : '#9E9E9E' }} />

                    <View>
                        <Icon
                            name={"user"}
                            type="feather"
                            color={step >= 2 ? '#000' : '#9E9E9E'}
                        />
                        <Text
                            style={{
                                ...styles.operText,
                                color: step >= 2 ? '#000' : '#9E9E9E'
                            }}>
                            Данные
                        </Text>
                    </View>

                    <View style={{ ...styles.separator, backgroundColor: step >= 3 ? '#000' : '#9E9E9E' }} />

                    <View>
                        <Icon name={"file-text"} type="feather" color={step >= 3 ? '#000' : '#9E9E9E'} />
                        <Text style={{ ...styles.operText, color: step >= 3 ? '#000' : '#9E9E9E' }}>Проверка</Text>
                    </View>
                </View>
                {
                    step === 0 ?
                        <Delivery
                            city={city}
                            setCity={setCity}
                            street={street}
                            setStreet={setStreet}
                            home={home}
                            setHome={setHome}
                            flat={flat}
                            setFlat={setFlat}
                            deliveryDate={deliveryDate}
                            deliveryPrice={deliveryPrice}
                        /> :
                        step === 1 ?
                            <Payment
                                setPayMethod={setPayMethod}
                            /> :
                        step === 2 ?
                            <Data
                                setFirstName={setFirstName}
                                setTel={setTel}
                                setEmail={setEmail}
                                firstName={firstName}
                                tel={tel}
                                email={email}
                            /> :
                            <Check 
                                city={city}
                                street={street}
                                home={home}
                                flat={flat}

                                payMethod={payMethod}

                                firstName={firstName}
                                tel={tel}
                                email={email}
                                comment={comment}
                                setComment={setComment} 
                                order={order}
                                deliveryDate={deliveryDate}

                            />
                }

                <View style={{ padding: 8, backgroundColor: '#fff' }}>
                    <Button
                        buttonStyle={{
                            paddingVertical: 12,
                            alignSelf: 'stretch',
                            borderRadius: 8,
                            backgroundColor: '#4D77FB'
                        }}
                        onPress={handleNextStep}
                        title={
                            step === 0 ? "Перейти к способам оплаты"
                            : step === 1 ? "Перейти к данным получателя"
                            : step === 2 ? "Проверить детали заказа"
                            : "Подтвердить заказ"
                        }
                        titleStyle={{ fontSize: 17, fontWeight: '500' }} />
                </View>
            </SafeAreaView >
        </KeyboardAvoidingView>
    )
}


const styles = StyleSheet.create({
    buttonText: {
        fontSize: 17,
        textAlign: 'center',
    },
    formInput: {
        fontSize: 17,
        paddingVertical: 14,
        paddingHorizontal: 16,
        backgroundColor: '#fff',
        marginBottom: 16,
        marginTop: 4,
        borderRadius: 8,
        fontWeight: '500'
    },
    text: {
        color: "#9E9E9E",
    },
    operText: {
        fontSize: 10,
        fontWeight: '500',
        lineHeight: 12,
        textAlign: 'center',
        marginTop: 4
    },
    separator: {
        height: 2,
        marginHorizontal: 4,
        marginTop: 12,
        borderRadius: 10,
        width: 38,
        marginHorizontal: 10
    }
});

export default CheckoutScreen
