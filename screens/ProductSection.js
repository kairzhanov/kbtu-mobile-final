import React from 'react'
import {
    StyleSheet,
    SafeAreaView,
    FlatList,
    Button,
    Text,
    Image,
    TouchableOpacity,
    View
} from "react-native";
import { Header, Icon, BottomSheet, ListItem } from "react-native-elements";
import { useSelector } from 'react-redux';
import CardHorizontal from '../components/card/CardHorizontal';
import CardVertical from '../components/card/CardVertical';
import {icons} from './../constants'


function ProductSection(props) {

    let products = props.route.params.products.products
    products = products ? products.filter(prod => prod!==null) : []
    const [col, setCol] = React.useState(true)

    const sortProducts = prods => {
        switch (currency) {
            case 'DWN':
                return prods.sort((a,b) => a.price + b.price)
            case 'UP':
                return prods.sort((a,b) => a.price - b.price)
            default:
                return prods;
        }
    }
    
    const [isVisible, setIsVisible] = React.useState(false);
    const [currency, setCurrency] = React.useState('DWN')
    const list = [
        { title: "По популярности", code: 'POP' },
        { title: "По рейтингу", code: 'RAT'},
        { title: "По новизне", code: 'NEW'},
        { title: "По возрастанию цены", code: 'UP'},
        { title: "По убыванию цены", code: 'DWN'}
    ];

    const renderItem = ({ item }) => {
        if (item && item != null) {
            // return <CardHorizontal item={item} navigation={props.navigation}/>
            switch (col) {
                case false:
                    return <CardHorizontal item={item} navigation={props.navigation}/>
                case true:
                    return <CardVertical item={item} navigation={props.navigation}/>
                
                default:
                    break;
            }
        }
    };
    return (

        <SafeAreaView style={styles.droidSafeArea}>
            <Header
                centerComponent={{
                    text: props.route.params.products.name,
                    style: { fontWeight: '600', fontSize: 17 },
                }}
                containerStyle={{
                    backgroundColor: "#F5F5F5",
                    paddingBottom: 20,
                }}
                statusBarProps={{
                    backgroundColor: "#F5F5F5",
                    barStyle: "dark-content",
                }}
                leftComponent={
                    <Icon
                        name='arrow-left'
                        type="feather"
                        onPress={() => props.navigation.goBack()}
                    />
                }
            />
            <View
                style={{
                    flexDirection:'row',
                    justifyContent:'space-between',
                    paddingHorizontal: 16,
                    paddingBottom:8,
                }}
            >

                <View
                    style={{
                        flexDirection:'row',
                        
                    }}
                >
                    <TouchableOpacity
                        style={{flexDirection:'row',alignItems:'center', marginRight:16}}
                        onPress={() => setIsVisible(true)}
                    >
                        <Image
                            source={icons.bar_chart}
                            resizeMode="contain"
                            style={{
                                width: 24,
                                height: 24
                            }}
                        />
                        <Text
                        style={{
                            fontWeight: '500',
                            fontSize: 15,
                            lineHeight: 18,
                            textAlign: 'center',
                            letterSpacing: -0.24,
                            marginLeft:8
                        }}
                        >Сортировать</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{flexDirection:'row',alignItems:'center'}}
                        onPress={() => props.navigation.navigate('Filter', {products: products})}
                    >
                        <Image
                            source={icons.sliders}
                            resizeMode="contain"
                            style={{
                                width: 24,
                                height: 24
                            }}
                        />
                        <Text
                        style={{
                            fontWeight: '500',
                            fontSize: 15,
                            lineHeight: 18,
                            textAlign: 'center',
                            letterSpacing: -0.24,
                            marginLeft:8
                        }}
                        >Фильтры</Text>
                    </TouchableOpacity>
                </View>

                <TouchableOpacity
                        onPress={() => setCol(!col)}
                >
                    {
                        col ?
                        <Image
                            source={icons.grid_vertical}
                            resizeMode="contain"
                            style={{
                                width: 24,
                                height: 24
                            }}
                        />
                        :
                        <Image
                            source={icons.grid}
                            resizeMode="contain"
                            style={{
                                width: 24,
                                height: 24
                            }}
                        />
                    }
                </TouchableOpacity>
            </View>
            {
                col &&
                <FlatList
                    data={sortProducts(products)}
                    renderItem={renderItem}
                    numColumns={2}
                    keyExtractor={(item) => item.id.toString() }
                    columnWrapperStyle={{paddingHorizontal:8, paddingVertical:4, justifyContent:'space-between'}}
                /> 
            }
            {
                !col &&
                <FlatList
                    data={sortProducts(products)}
                    renderItem={renderItem}
                    keyExtractor={(item) => item.id.toString() }
                /> 
            }
                <BottomSheet
                isVisible={isVisible}
                containerStyle={{  padding: 8 }}
                modalProps={{ animationType: 'fade' }}

            >
                {
                    list.map((l, i) => (
                        <ListItem
                            key={i}
                            containerStyle={[
                                i===0 && {borderTopLeftRadius:8, borderTopRightRadius:8}, 
                                i===list.length-1 && {borderBottomLeftRadius:8, borderBottomRightRadius:8},
                                {paddingVertical:18}]}
                            onPress={()=>{
                                l.code!==currency && setCurrency(l.code)
                                l.code!==currency && setIsVisible(false)
                            }}
                            bottomDivider
                        >

                            <Icon name={'check'} color={l.code!==currency  && '#fff'} type="feather"/>
                            <ListItem.Content>
                                <ListItem.Title style={{...styles.currencyText, color: l.code ===currency ? '#BDBDBD' : '#212121'}}>{l.title}</ListItem.Title>
                            </ListItem.Content>

                        </ListItem>
                ))}
                <TouchableOpacity style={{
                    paddingVertical: 16,
                    alignItems: 'center',
                    backgroundColor: '#fff',
                    marginVertical: 8,
                    borderRadius: 8

                }} 
                onPress ={() => setIsVisible(false)} >
                    <Text style={{
                        color: '#4D77FB',
                        fontSize: 20,
                        fontWeight: '600'
                    }} >Отмена</Text>
                </TouchableOpacity>
            </BottomSheet>
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({
    droidSafeArea: {
        flex: 1,
        //paddingTop: Platform.OS === "android" ? 25 : 0,
        backgroundColor: "#F5F5F5",
    },
    currencyText: {
        fontWeight:'400',
        fontSize: 17
    }
});


export default ProductSection