import { StatusBar } from 'expo-status-bar'
import React, { useEffect, useState } from 'react'
import { SafeAreaView } from 'react-native'
import { StyleSheet, Text, View, TouchableOpacity, FlatList } from 'react-native'
import axios from 'axios'
import CardHorizontal from '../components/card/CardHorizontal'
import CardVertical from '../components/card/CardVertical'
import { Icon, Header } from 'react-native-elements'

const SearchResults = ({route, navigation}) => {

    const [res, setRes] = useState([])
    const [col, setCol] = React.useState(false)

    useEffect(() => {
        axios.get(`https://doxjo-10749.firebaseio.com/products.json?orderBy="name"&startAt="${route.params.search}"&endAt="${route.params.search}\uf8ff"`)
        .then(function (response) {
            // handle success
            var myData = Object.keys(response.data).map(key => {
                return response.data[key];
            })
            setRes(myData)
            
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
    }, [])

    const renderItem = ({item}) => {
        if (item && item != null) {
            switch (col) {
                case false:
                    return <CardHorizontal item={item} navigation={navigation}/>
                case true:
                    return <CardVertical item={item} navigation={navigation}/>
                
                default:
                    break;
            }
        }
    }
    
    return (
        <SafeAreaView style={{flex:1}}>
            <Header
                centerComponent={{
                    text: 'Результаты поиска',
                    style: { fontWeight: '600', fontSize: 17 },
                }}
                containerStyle={{
                    backgroundColor: "#F5F5F5",
                    paddingBottom: 20,
                }}
                statusBarProps={{
                    backgroundColor: "#F5F5F5",
                    barStyle: "dark-content",
                }}
                leftComponent={
                    <Icon
                        name='arrow-left'
                        type="feather"
                        onPress={() => navigation.goBack()}
                    />
                }
            />
            <FlatList
                data={res}
                keyExtractor={item => item.id.toString()}
                renderItem={renderItem}
            />
        </SafeAreaView>
    )
}

export default SearchResults

const styles = StyleSheet.create({})
