import React, { useState } from 'react'
import { ScrollView } from 'react-native';
import { SafeAreaView, StyleSheet, Text, View, TouchableOpacity, StatusBar,Switch } from 'react-native'
import { Header, ListItem } from 'react-native-elements';

const Filter = (props) => {
    
    console.log(props.route.params.products.length);
    
    const [isNew, setIsNew] = useState(false)
    const [isDiscount, setIsDiscount] = useState(false)
    const [category, setCategory] = useState(13)
    const [sale, setSale] = useState()
    const [productClass, setProductClass] = useState()
    const [productPrice, setProductPrice] = useState()
    const [productBrand, setProductBrand] = useState()
    const [productRating, setProductRating] = useState()
    const [productSeller, setProductSeller] = useState()

    const toggleNew = () => setIsNew(previousState => !previousState);
    const toggleDiscount = () => setIsDiscount(previousState => !previousState);

    return (
        <SafeAreaView style={{flex:1}}>
            <Header 
                leftComponent={
                    <TouchableOpacity
                        onPress={() => props.navigation.goBack()}>
                        <Text style={{color:'#007AFF', fontSize: 17, fontWeight:'400'}}>Отмена</Text>
                    </TouchableOpacity>
                }
                centerComponent={{
                    text: "Фильтр",
                    style: { fontWeight: "600", fontSize: 17 },
                }}
                rightComponent={
                    <TouchableOpacity>
                        <Text style={{color:'#007AFF', fontSize: 17, fontWeight:'400', width:80}}>Сбросить</Text>
                    </TouchableOpacity>
                }
                rightContainerStyle={{
                    
                }}
                containerStyle={{
                    backgroundColor: "#F5F5F5",
                    paddingBottom: 12,
                }}
                statusBarProps={{
                    backgroundColor: "#F5F5F5",
                    barStyle: "dark-content",
                }}
            />

            <ScrollView
                style={{
                    padding:16,
                }}
            >

                <ListItem
                    containerStyle={{ borderTopLeftRadius: 4, borderTopRightRadius: 4 }}
                    bottomDivider
                >
                    <ListItem.Content>
                        <ListItem.Title style={{fontSize:17, fontWeight:'400'}}>Новинка</ListItem.Title>
                    </ListItem.Content>
                    <Switch
                        trackColor={{ false: "#D9E2EC", true: "#81b0ff" }}
                        thumbColor={"#fff"}
                        ios_backgroundColor="#D9E2EC"
                        onValueChange={toggleNew}
                        value={isNew}
                    />
                </ListItem>

                <ListItem
                    containerStyle={{ borderBottomLeftRadius: 4, borderBottomRightRadius: 4, marginBottom: 8 }}
                >
                    <ListItem.Content>
                        <ListItem.Title style={{fontSize:17, fontWeight:'400'}}>Товар со скидкой</ListItem.Title>
                    </ListItem.Content>
                    <Switch
                        trackColor={{ false: "#D9E2EC", true: "#81b0ff" }}
                        thumbColor={"#fff"}
                        ios_backgroundColor="#D9E2EC"
                        onValueChange={toggleDiscount}
                        value={isDiscount}
                    />
                </ListItem>


                <ListItem
                    containerStyle={{ borderTopLeftRadius: 4, borderTopRightRadius: 4 }}
                    bottomDivider
                >
                    <ListItem.Content>
                        <ListItem.Title style={{fontSize:17, fontWeight:'400'}}>Категории</ListItem.Title>
                        <ListItem.Subtitle style={{fontSize:10, fontWeight:'500', color:'#9E9E9E'}}>Телефоны и гаджеты</ListItem.Subtitle>
                    </ListItem.Content>
                </ListItem>
                <ListItem
                    containerStyle={{ borderBottomLeftRadius: 4, borderBottomRightRadius: 4, paddingVertical:18 }}
                    bottomDivider
                >
                    <ListItem.Content>
                        <ListItem.Title style={{fontSize:17, fontWeight:'400'}}>Акции</ListItem.Title>
                        {/* <ListItem.Subtitle style={{fontSize:10, fontWeight:'500', color:'#9E9E9E'}}>10 000 - 100 000</ListItem.Subtitle> */}
                    </ListItem.Content>
                </ListItem>
                <ListItem
                    containerStyle={{ borderBottomLeftRadius: 4, borderBottomRightRadius: 4, paddingVertical:18 }}
                    bottomDivider
                >
                    <ListItem.Content>
                        <ListItem.Title style={{fontSize:17, fontWeight:'400'}}>Класс</ListItem.Title>
                        {/* <ListItem.Subtitle style={{fontSize:10, fontWeight:'500', color:'#9E9E9E'}}>10 000 - 100 000</ListItem.Subtitle> */}
                    </ListItem.Content>
                </ListItem>
                <ListItem
                    containerStyle={{ borderBottomLeftRadius: 4, borderBottomRightRadius: 4, paddingVertical:18 }}
                    bottomDivider
                >
                    <ListItem.Content>
                        <ListItem.Title style={{fontSize:17, fontWeight:'400'}}>Цены</ListItem.Title>
                        <ListItem.Subtitle style={{fontSize:10, fontWeight:'500', color:'#9E9E9E'}}>10 000 - 100 000</ListItem.Subtitle>
                    </ListItem.Content>
                </ListItem>
                <ListItem
                    containerStyle={{ borderBottomLeftRadius: 4, borderBottomRightRadius: 4, paddingVertical:18 }}
                    bottomDivider
                >
                    <ListItem.Content>
                        <ListItem.Title style={{fontSize:17, fontWeight:'400'}}>Бренд</ListItem.Title>
                        {/* <ListItem.Subtitle style={{fontSize:10, fontWeight:'500', color:'#9E9E9E'}}>{'item.geo'}</ListItem.Subtitle> */}
                    </ListItem.Content>
                </ListItem>
                <ListItem
                    containerStyle={{ borderBottomLeftRadius: 4, borderBottomRightRadius: 4, paddingVertical:18 }}
                    bottomDivider
                >
                    <ListItem.Content>
                        <ListItem.Title style={{fontSize:17, fontWeight:'400'}}>Рейтинг</ListItem.Title>
                        {/* <ListItem.Subtitle style={{fontSize:10, fontWeight:'500', color:'#9E9E9E'}}>{'item.geo'}</ListItem.Subtitle> */}
                    </ListItem.Content>
                </ListItem>
                <ListItem
                    containerStyle={{ borderBottomLeftRadius: 4, borderBottomRightRadius: 4 }}
                >
                    <ListItem.Content>
                        <ListItem.Title style={{fontSize:17, fontWeight:'400'}}>Продавцы</ListItem.Title>
                        {/* <ListItem.Subtitle style={{fontSize:10, fontWeight:'500', color:'#9E9E9E'}}>{'item.geo'}</ListItem.Subtitle> */}
                    </ListItem.Content>
                </ListItem>


            </ScrollView>
            
        </SafeAreaView>
    )
}

export default Filter

const styles = StyleSheet.create({})
