import React from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
  SafeAreaView
} from "react-native";
import { Header, Icon } from "react-native-elements";
import { FlatList } from "react-native";
import { connect, useSelector } from 'react-redux';

const OrdersScreen = (props) => {

  const orders = useSelector(state => state.cart.cartReducer.orders)
  const renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => props.navigation.navigate("OrderDetail", {
          order: item
        })}
      >
        <Image
          source={{ uri: item.image }}
          resizeMode="cover"
          style={styles.adsImageCover}
        />
        <View style={{ flex: 1, justifyContent: "space-between", marginLeft: 8 }}>
          <View>
            <Text style={{ fontWeight: '500', fontSize: 13 }}>{item.name}</Text>
            <Text style={{ fontWeight: '500', fontSize: 10, color: '#9E9E9E' }}>Заказ №{item.orderNumber}</Text>
              {
                item.orderStatus === 'inprocessing' ?
                <Text style={{ fontWeight: '600', fontSize: 12, color: '#FA8C00', lineHeight: 14.32 }}>
                    В обработке
                </Text> :
                item.orderStatus === 'delivered' ?
                <Text style={{ fontWeight: '600', fontSize: 12, color: '#66BB6A', lineHeight: 14.32 }}>
                    Выдан
                </Text>:
                <Text style={{ fontWeight: '600', fontSize: 12, color: '#F44336', lineHeight: 14.32 }}>
                    Отменен
                </Text>

              }
          </View>
          <View>
            {
              item.qty >1 && <Text style={{ fontWeight: '500', fontSize: 10, color: '#9E9E9E' }}>{item.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")} $ х {item.qty}шт.</Text>
            }
            <Text style={{ fontWeight: '600', fontSize: 12 }}>{(item.price * item.qty).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")} $</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <SafeAreaView style={{ flex: 1 }}>

      <Header
        leftComponent={
          <Icon
            name='arrow-left'
            type="feather"
            onPress={() => props.navigation.goBack()}
          />
        }
        centerComponent={{
          text: 'Мои заказы',
          style: { fontWeight: '600', fontSize: 17 },
        }}
        containerStyle={{
          backgroundColor: "#F5F5F5",
          paddingVertical: 12,
        }}
        statusBarProps={{
          backgroundColor: "#F5F5F5",
          barStyle: "dark-content",
        }}
      />
      <FlatList
        data={orders}
        renderItem={renderItem}
        keyExtractor={item => item.orderNumber.toString()}
        showsHorizontalScrollIndicator={false}
      />
    </SafeAreaView>
  );
};


const styles = StyleSheet.create({
  adsImageCover: {
    width: 84,
    height: 84,
    borderRadius: 4,
  },
  container: {
    flex: 1,
    margin: 5,
    backgroundColor: "#fff",
    flexDirection: "row",
    borderRadius: 12,
    marginHorizontal: 16,
    height: 100,
    padding: 8
  },
  checkoutContainer: {
    flexDirection: "row",
    justifyContent: 'space-between',
    height: 60,
    alignItems: 'center',
    paddingHorizontal: 8,
    backgroundColor: '#fff',
  }
});

export default OrdersScreen