import React, { useState, useEffect } from "react";
import { StyleSheet, SafeAreaView, View, TouchableOpacity, Image, Text } from "react-native";
import { SearchBar } from "react-native-elements";
import { FlatList } from "react-native";
import { StatusBar } from "react-native";

function CatalogScreen({ navigation }) {

  const [cats, setCats] = useState([])
  const [isSearchVisible, setIsSearchVisible] = useState(false)

  useEffect(() => {
    fetch("https://doxjo-10749.firebaseio.com/categories.json")
      .then(res => res.json())
      .then(
        (result) => {
          setCats(result);
        },
        (error) => {
          setError(error);
        }
      )
      return () => {
        setCats([])}
  }, [])
  const renderItem = ({ item }) => {
   if (item && item!=null){
    return (
      <TouchableOpacity
        style={styles.catContainer}
        onPress={() => { navigation.navigate( "ProductSection", { products: item } ) }}
      >
        <Image
          source={{ uri: item.image }}
          resizeMode="cover"
          style={styles.catImageCover}
        />
        <View style={styles.textContainer}>
          <Text style={styles.textTitle}>{item.name}</Text>
        </View>
      </TouchableOpacity>
    )}
  }
  return (
    <SafeAreaView style={[styles.droidSafeArea, isSearchVisible && {backgroundColor:'rgba(0, 0, 0, 0.3)'}]}>
      <StatusBar hidden={false}/>
      <SearchBar
        platform="ios"
        placeholder="Поиск"
        inputContainerStyle={{
          borderRadius: 8,
          backgroundColor: "#fff",
          height: 40,
        }}
        containerStyle={[{
          backgroundColor: "#F5F5F5",
          height: 40,
          marginVertical: 4,
          marginHorizontal: 8
        }, isSearchVisible && {backgroundColor:'rgba(0, 0, 0, 0.0)'}]}
        onFocus={() => setIsSearchVisible(true)}
        onBlur={() => setIsSearchVisible(false)}
      />
      <FlatList
        data={cats}
        renderItem={renderItem}
        keyExtractor={item =>item?item.id.toString():''}
        contentContainerStyle={isSearchVisible && {opacity:0}}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  droidSafeArea: {
    flex: 1,
    backgroundColor: "#F5F5F5",
    paddingTop: Platform.OS === 'android' ? 30 : 0,
  },
  catImageCover: {
    width: 90,
    height: 60,
    borderBottomLeftRadius: 4,
    borderTopLeftRadius: 4,
  },
  catContainer: {
    marginVertical: 4,
    backgroundColor: '#fff',
    flexDirection: 'row',
    borderRadius: 4,
    marginHorizontal: 16,
    height: 60
  },
  textContainer:{
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 16
  },
  textTitle:{
    fontWeight: '600',
    fontSize: 12
  }
});

export default CatalogScreen;
