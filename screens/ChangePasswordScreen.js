import React, { useState,useEffect } from 'react';
import { View, Text, TextInput, StyleSheet, Alert, SafeAreaView, ScrollView,TouchableOpacity } from 'react-native';
import { loggingOut } from '../API/firebaseMethods';
import {
  Header,
  Button,
  ListItem,
  Icon
} from "react-native-elements";
import { changePassword } from '../actions/userActions';
import { useDispatch } from 'react-redux';

const PersonalDataScreen = (props) => {

    const [oldPassword, setOldPassword] = useState('')
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');

    const [secureOldPassword, setsecureOldPassword] = useState(true)
    const [secure, setSecure] = useState(true)
    const [secureConfirm, setSecureConfirm] = useState(true)

    const dispatch = useDispatch()
    
    const emptyState = () => {
        setPassword('');
        setConfirmPassword('');
        setOldPassword('')
      };
    
      const handlePress = () => {
        if (!password) {
          Alert.alert('Password field is required.');
        } else if (!confirmPassword) {
          setPassword('');
          Alert.alert('Confirm password field is required.');
        } else if (password !== confirmPassword) {
          Alert.alert('Password does not match!');
        } else {
            dispatch(changePassword(password))
            props.navigation.goBack()
          emptyState();
        }
      };
    return (
        <SafeAreaView style={{flex:1}}>
            <Header 
                leftComponent={
                    <Icon 
                        name='arrow-left' 
                        type="feather" 
                        onPress={() => props.navigation.goBack()} 
                    />
                }
                centerComponent={{
                    text: "Изменить пароль",
                    style: { fontWeight: "600", fontSize: 17 },
                }}
                rightComponent={
                    <TouchableOpacity onPress={handlePress}>
                        <Text style={{color:'#007AFF', fontSize: 17, fontWeight:'500'}}>Готово</Text>
                    </TouchableOpacity>
                }
                containerStyle={{
                    backgroundColor: "#F5F5F5",
                    paddingBottom: 12,
                }}
                statusBarProps={{
                    backgroundColor: "#F5F5F5",
                    barStyle: "dark-content",
                }}
            />
            <ScrollView style={{ padding: 16 }} >

            <Text style={styles.text}>Текущий пароль</Text>
                <View style={{
                  flexWrap: 'wrap',
                  flexDirection: 'row'
                }}>
                  <TextInput
                      style={styles.passwordInput}
                      placeholder="Введите текущий пароль"
                      autoCapitalize="none"
                      secureTextEntry={secureOldPassword}
                      value={oldPassword}
                      onChangeText={(oldPassword) => setOldPassword(oldPassword)}
                  />
                    <Icon
                      name={secureOldPassword?'eye':'eye-off'}
                      type="feather"
                      color='#BDBDBD'
                      containerStyle={styles.secureIcon}
                      onPress={() => setsecureOldPassword(!secureOldPassword)}
                    />
                </View>

                <Text style={styles.text}>Новый пароль</Text>
                <View style={{
                  flexWrap: 'wrap',
                  flexDirection: 'row'
                }}>
                <TextInput 
                    style={styles.passwordInput}
                    placeholder="Придумайте новый пароль"
                    value={password}
                    secureTextEntry={secure}
                    onChangeText={password => setPassword(password)}
                    autoCapitalize="none"
                />
                  <Icon
                      name={secure?'eye':'eye-off'}
                      type="feather"
                      color='#BDBDBD'
                      containerStyle={styles.secureIcon}
                      onPress={() => setSecure(!secure)}
                    />
                </View>

                <Text style={styles.text}>Подтвердите новый пароль</Text>
                <View style={{
                  flexWrap: 'wrap',
                  flexDirection: 'row'
                }}>
                  <TextInput
                      style={styles.passwordInput}
                      placeholder="Повторите новый пароль"
                      value={confirmPassword}
                      onChangeText={confirmPassword=> setConfirmPassword(confirmPassword)}
                      autoCapitalize="none"
                      secureTextEntry={secureConfirm}
                  />
                  <Icon
                      name={secureConfirm?'eye':'eye-off'}
                      type="feather"
                      color='#BDBDBD'
                      containerStyle={styles.secureIcon}
                      onPress={() => setSecureConfirm(!secureConfirm)}
                    />
                </View>

            </ScrollView>
        </SafeAreaView>
    )
}

export default PersonalDataScreen

const styles = StyleSheet.create({
    button: {
      paddingVertical: 12,
      borderWidth: 2,
      borderRadius: 8,
      alignSelf: 'center',
    },
    buttonText: {
      fontSize: 17,
      textAlign: 'center',
    },
    formInput: {
      fontSize: 17,
      padding: 14,
      backgroundColor: '#fff',
      marginBottom: 16,
      marginTop: 4,
      borderRadius: 12,
      fontWeight: '500',
      lineHeight: 20.29
    },
    text: {
      color: "#9E9E9E",
      fontSize: 13,
      fontWeight: '400'
    },
    passwordInput: {
      flex: 1,
      fontSize: 17,
      padding: 14,
      backgroundColor: '#fff',
      marginBottom: 10,
      marginTop: 4,
      borderTopLeftRadius: 8,
      borderBottomLeftRadius: 8
    },
    secureIcon: {
      padding: 12,
      backgroundColor: '#fff',
      marginBottom: 10,
      marginTop: 4,
      borderBottomRightRadius: 8,
      borderTopRightRadius: 8
    }
  });