import { LinearGradient } from "expo-linear-gradient";
import React, { useEffect, useState } from "react";
import {
    View,
    Text,
    ScrollView,
    SafeAreaView,
    FlatList,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import { ListItem, Button, Icon, Header } from 'react-native-elements'
import { useDispatch, useSelector } from 'react-redux';
import ImageCarousel from "../components/ProductDetail/ImageCarousel";
import { addToCart, deleteFromCart } from './../actions/CartActions';


const BookDetail = (props) => {

    const [book, setBook] = useState(props.route.params.book);
    const dispatch = useDispatch()
    const cart = useSelector(state => state.cart.cartReducer)
    let searched = props.route.params.book
    if (book) {
        searched = cart.cart.find(item => item.id === book.id)
    }

    
    useEffect(() => {
        if (!!searched) {
            setBook(searched)
        }
        else {
            setBook(props.route.params.book)
        }
        return () => {
            setBook(null)
        }
    }, [book,cart])

    const numberWithSpaces = x => x && x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")
    
    const renderItem = (item) => {

        if (item['item'] !== null) {
            return (
                <View style={styles.colorContainer} >
                    <View
                        style={{
                            height: 36,
                            width: 36,
                            backgroundColor: item['item']['value'],
                            borderRadius: 2
                        }}
                    >
                    </View>
                </View>
            )
        }

    }

    const [selectedColor, setSelectedColor] = React.useState()
    
    if (book) {
        return (
            <SafeAreaView style={{ flex: 1 }} >
                <Header
                    leftComponent={
                        <Icon
                            name='arrow-left'
                            type="feather"
                            onPress={() => props.navigation.goBack()}
                        />
                    }

                    rightComponent={
                        <Icon
                            name='share'
                            type="feather"
                            onPress={() => props.navigation.goBack()}
                        />
                    }
                    containerStyle={{
                        backgroundColor: "transparent",
                        paddingVertical: 12,
                    }}
                    statusBarProps={{
                        backgroundColor: "#F5F5F5",
                        barStyle: "dark-content",
                    }}
                />
                
                <ScrollView>

                    <View style={{ backgroundColor: 'white', marginVertical: 5 }} >
                        {
                            book.images && <ImageCarousel images={book.images}/>
                        }

                        <View style={{ marginVertical: 8, padding:16 }} >
                            <Text
                                numberOfLines={3}
                                style={{
                                    fontSize: 17,
                                    fontWeight: '500'
                                }}
                            >
                                {book.name}
                            </Text>

                            {
                                book['attributes'] && 
                                <View>
                                    <Text style={{ color: '#757575', paddingVertical: 4 }}>Цвет: </Text>
                                    <View style={{ flex: 1, flexDirection: 'row', paddingVertical: 5 }} >
                                        <FlatList
                                            data={book['attributes']['colors']}
                                            renderItem={renderItem}
                                            keyExtractor={(item) => item && item.value}
                                            horizontal
                                            showsHorizontalScrollIndicator={false}
                                        />
                                    </View>
                                </View>
                            }
                            <View 
                                style={{
                                    justifyContent:'space-between',
                                    flexDirection:'row',
                                    alignItems:'center'
                                }}
                            >
                                <Text style={{fontSize: 27, fontWeight:'600', letterSpacing:-.24, lineHeight:32 }} >
                                    {numberWithSpaces(book.price)} $
                                </Text>
                                <View>
                                    <Text
                                        style={{
                                            color:'#E53935',
                                            fontWeight:'500',
                                            fontSize:12,
                                            lineHeight:14
                                        }}
                                    >
                                        {'7 777 777'} ₸
                                    </Text>
                                    <Text
                                        style={{
                                            fontSize:12,
                                            color:'#757575',
                                            lineHeight:14
                                        }}
                                    >
                                        при заказе от {10} шт.
                                    </Text>
                                </View>
                            </View>
                            <View
                                style={{
                                    flexDirection:'row'
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize:17,
                                        color:'#757575',
                                        letterSpacing:0.5,
                                        lineHeight:20,
                                        marginRight:6
                                    }}
                                >
                                    Отправим 
                                </Text>
                                <Text
                                    style={{
                                        fontSize:17,
                                        color:'#E53935',
                                        letterSpacing:0.5,
                                        lineHeight:20
                                    }}
                                >
                                    7 ноября
                                </Text>
                            </View>
                        </View>

                    </View>

                    <View style={{ backgroundColor: 'white', padding: 16, borderRadius: 12, marginVertical: 5 }} >
                        <Text style={{ fontSize: 20, marginVertical: 10 }}>
                            Характеристики
                        </Text>
                        {book['attributes'] && Object.keys(book['attributes']['specs']).map((har, i) => har && (
                            <ListItem key={i} bottomDivider containerStyle={{paddingHorizontal:0, paddingVertical: 8}}>
                                <ListItem.Content style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }} >
                                    <Text style={{fontSize:15, fontWeight:'500'}} >{har}</Text>
                                    <Text>{book['attributes']['specs'][har]}</Text>
                                </ListItem.Content>
                            </ListItem>
                        ))}
                    </View>

                    <View style={{ backgroundColor: 'white', padding: 16, borderRadius: 12, marginVertical: 5 }} >
                        <Text style={{ fontSize: 20, marginVertical: 10 }}>Описание товара</Text>
                        <Text style={styles.description} >
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                            Commodo proin aenean non ultrices sed. Pellentesque tellus
                            nec lacinia natoque gravida egestas a ante urna. Cras 
                            ut dictumst urna aliquet. At elementum vitae lectus aliquet 
                            sit erat habitasse at. Dignissim amet consectetur interdum hac. 
                            Quisque eu cum adipiscing ac amet, senectus dui. Sit et parturient
                            volutpat tellus. Tristique urna fames etiam ligula egestas 
                            dignissim viverra non. Sit diam condimentum ridiculus ac nulla.
                            Metus laoreet sodales suspendisse morbi ut accumsan euismod.
                            Aenean laoreet odio ut eu mauris. Amet laoreet orci sit.
                        </Text>
                    </View>

                </ScrollView>

                <View>
                    <View style={{
                        padding:8,
                        height: 60,
                        backgroundColor:'#fff'
                    }}>
                        {
                            book.qty && book.qty>0 ?
                            <View style={{flexDirection:'row', justifyContent:'space-between'}} >

                                <View style={{flex:1}}>
                                    <TouchableOpacity 
                                        style={styles.goToCartButton}
                                        onPress={() => props.navigation.navigate('Cart')}
                                    >
                                        <Icon 
                                            name='shopping-cart' 
                                            type="feather" 
                                            color={'#4D77FB'}
                                        />
                                        <Text style={styles.goToCartButtonTitle}>Перейти в корзину</Text>
                                    </TouchableOpacity>
                                </View>

                                <View 
                                    style={{ 
                                        backgroundColor: '#E9ECFF', 
                                        flexDirection: 'row', 
                                        padding: 7, 
                                        borderRadius: 8, 
                                        alignItems: 'center' 
                                    }}
                                >
                                    <Icon 
                                        name='minus'
                                        type='feather'
                                        color='#4D77FB'
                                        onPress={() => dispatch(deleteFromCart(book))}
                                    />
                                    <Text 
                                        style={{ 
                                            marginHorizontal: 10, 
                                            fontWeight: '600', 
                                            fontSize: 17, 
                                            color:'#4D77FB' 
                                        }}
                                    >
                                        {book.qty && book.qty}
                                    </Text>
                                    <Icon 
                                        name='plus' 
                                        type='feather'
                                        color='#4D77FB'
                                        onPress={() => dispatch(addToCart(book))} />
                                </View>
                                
                            </View>
                            :
                            <Button
                                title="Добавить в корзину"
                                onPress={() => dispatch(addToCart(book))}
                                buttonStyle={styles.btnAdd} 
                            />
                        }
                    </View>
                </View>

            </SafeAreaView>
        )
    } else {
        return (<></>)
    }

}

export default BookDetail

const styles = StyleSheet.create({
    colorContainer: {
        flex: 1,
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        width: 40,
        marginHorizontal: 5,
        borderColor: '#E0E0E0',
        borderWidth: 1
    },
    description: {
        fontWeight:'400', 
        fontSize: 15, 
        letterSpacing: -0.24,
        color:'#212121'
    },
    btnAdd: {
        paddingVertical: 12,
        borderRadius: 8,
        backgroundColor: '#4D77FB',
        fontSize: 17,
        fontWeight: '500',
        alignSelf: 'stretch'
    },
    goToCartButton: {
        backgroundColor:'#E9ECFF', 
        padding:10, 
        marginRight:8, 
        borderRadius:8,
        flexDirection:'row', 
        alignItems:'center'
    },
    goToCartButtonTitle: {
        fontWeight:'500', 
        fontSize:17, 
        color:'#4D77FB', 
        paddingLeft:8 
    }
})