import React, {useState, useEffect} from 'react'
import { ScrollView, FlatList, TouchableOpacity, View, Text  } from 'react-native'
import { SearchBar, ListItem, Icon, Header, Divider } from 'react-native-elements'
import { SafeAreaView } from 'react-native-safe-area-context'
import { useDispatch } from 'react-redux'
import { changeCity } from '../actions/CartActions'

const cities = [
    {
        'id':0,
        'city': 'Алматы',
        'geo': 'Казахстан'
    },
    {
        'id':1,
        'city': 'Нур-Султан',
        'geo': 'Казахстан'
    },
    {
        'id':2,
        'city': 'Шымкент',
        'geo': 'Казахстан'
    },
    {
        'id':3,
        'city': 'Талгар',
        'geo': 'Талагрский р-н, Алматинский обл., Казахстан'
    },
    {
        'id':4,
        'city': 'Алматы',
        'geo': 'Казахстан'
    },
    {
        'id':5,
        'city': 'Нур-Султан',
        'geo': 'Казахстан'
    },
    {
        'id':6,
        'city': 'Шымкент',
        'geo': 'Казахстан'
    },
    {
        'id':7,
        'city': 'Талгар',
        'geo': 'Талагрский р-н, Алматинский обл., Казахстан'
    },
    {
        'id':8,
        'city': 'Алматы',
        'geo': 'Казахстан'
    },
    {
        'id':9,
        'city': 'Нур-Султан',
        'geo': 'Казахстан'
    },
    {
        'id':10,
        'city': 'Шымкент',
        'geo': 'Казахстан'
    },
    {
        'id':11,
        'city': 'Талгар',
        'geo': 'Талагрский р-н, Алматинский обл., Казахстан'
    },
]

const SelectCityScreen = (props) => {

    const [data, setData] = useState()
    const dispatch = useDispatch()
    const [value, setValue] = useState()
    const arrayholder = cities;
    const searchFilterFunction = text => {
        setValue(text)
    
        const newData = arrayholder.filter(item => {
          const itemData = `${item.city.toUpperCase()} ${item.geo.toUpperCase()}`;
          const textData = text.toUpperCase();
    
          return itemData.indexOf(textData) > -1;
        });
        setData(newData)
      };
      useEffect(() => {
          setData(cities)
          return () => setData([])
      }, [])

      const handlePress = (city) => {
          dispatch(changeCity(city))
          props.navigation.goBack()
      }
    const renderItem = ({item}) => {
        return (
            <TouchableOpacity
                onPress={() =>handlePress(item)}
            >
                <ListItem
                    containerStyle={{ borderTopLeftRadius: 4, borderTopRightRadius: 4 }}
                    bottomDivider
                >
                    <ListItem.Content>
                        <ListItem.Title style={{fontSize:17, fontWeight:'400'}}>{item.city}</ListItem.Title>
                        <ListItem.Subtitle style={{fontSize:10, fontWeight:'500', color:'#9E9E9E'}}>{item.geo}</ListItem.Subtitle>
                    </ListItem.Content>
                </ListItem>
            </TouchableOpacity>
        );
    };
    return (
        <SafeAreaView style={{flex:1}}>
            <Header 
                leftComponent={
                    <Icon 
                        name='arrow-left' 
                        type="feather" 
                        onPress={() => props.navigation.goBack()} 
                    />
                }
                centerComponent={{
                    text: "Выберите город",
                    style: { fontWeight: "600", fontSize: 17 },
                }}
                containerStyle={{
                    backgroundColor: "#F5F5F5",
                    paddingBottom: 12,
                }}
                statusBarProps={{
                    backgroundColor: "#F5F5F5",
                    barStyle: "dark-content",
                }}
            />
            <SearchBar
                platform="ios"
                placeholder="Поиск"
                inputContainerStyle={{
                    borderRadius: 8,
                    backgroundColor: "#fff",
                    height: 40,
                }}
                containerStyle={{
                    backgroundColor: "#F5F5F5",
                    height: 40,
                    marginVertical: 4,
                    marginHorizontal: 8
                }}
                onChangeText={text => searchFilterFunction(text)}
                value={value}
            />
            <View style={{marginHorizontal:16, flex:1, borderRadius:40}}>
                <FlatList
                    data={data}
                    renderItem={(item, index) => renderItem(item, index)}
                    keyExtractor={(item) => `${item.id}`}
                    showsVerticalScrollIndicator={false}
                    ItemSeparatorComponent={() => (<Divider style={{paddingLeft:16}}/>)}
                />
            </View>
        </SafeAreaView>
    )
}

export default SelectCityScreen
