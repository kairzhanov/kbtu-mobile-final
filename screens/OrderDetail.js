import React from 'react'
import { ScrollView } from 'react-native'
import { SafeAreaView, StyleSheet, Text, View, Image } from 'react-native'
import { Icon, Header } from 'react-native-elements'
import Canceled from '../components/order/Canceled'
import Delivered from '../components/order/Delivered'
import InProcessing from '../components/order/InProcessing'

const OrderDetail = (props) => {
    const order = props.route.params.order
    const status = 'processing'
    return (
        <SafeAreaView style={{flex:1}}>
            <Header
                leftComponent={
                    <Icon
                        name='arrow-left'
                        type="feather"
                        onPress={() => props.navigation.goBack()}
                    />
                }
                centerComponent={{
                    text: `Заказ №${order.orderNumber}`,
                    style: { fontWeight: '600', fontSize: 17 },
                }}
                containerStyle={{
                    backgroundColor: "#F5F5F5",
                    paddingVertical: 12,
                }}
                statusBarProps={{
                    backgroundColor: "#F5F5F5",
                    barStyle: "dark-content",
                }}
            />
            <ScrollView>
                {
                    order.orderStatus === 'delivered' ?
                    <Delivered orderDay={order.deliveryDate}/> :
                    order.orderStatus === 'canceled' ?
                    <Canceled orderDay={order.deliveryDate}/> :
                    <InProcessing orderDay={order.deliveryDate}/>
                }

                <View style={styles.container}>
                    <Image
                        source={{ uri: order.image }}
                        resizeMode="cover"
                        style={styles.adsImageCover}
                    />
                    <View style={{ flex: 1, justifyContent: "space-between", marginLeft: 8 }}>
                        <View>
                            <Text style={{ fontWeight: '500', fontSize: 13 }}>{order.name}</Text>
                            <Text style={{ fontWeight: '500', fontSize: 10, color: '#9E9E9E', lineHeight: 12 }}>{order.description}</Text>
                        </View>
                        <View>
                            {
                                order.qty >1 && <Text style={{ fontWeight: '500', fontSize: 10, color: '#9E9E9E' }}>{order.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")} $ х {order.qty}шт.</Text>
                            }
                            
                            <Text style={{ fontWeight: '600', fontSize: 12 }}>{(order.price * order.qty).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")} $</Text>
                        </View>
                    </View>
                </View>

                <View style={{...styles.container, padding: 16, flex:1, flexDirection:'column'}}>

                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Icon name='truck' type="feather" containerStyle={{marginRight:16}} color='#757575'/>
                        <View>
                            <Text style={styles.optionTitle}>Стоимость доставки</Text>
                            <Text style={styles.optionText}>{order.deliveryPrice}</Text>
                        </View>
                    </View>

                    <View style={{flexDirection:'row', alignItems:'center', marginTop:8}}>
                        <Icon name='box' type="feather" containerStyle={{marginRight:16}} color='#757575'/>
                        <View>
                            <Text style={styles.optionTitle}>Стоимость товара</Text>
                            <Text style={styles.optionText}>{(order.price * order.qty).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")} ₸</Text>
                        </View>
                    </View>

                    <View style={{flexDirection:'row', alignItems:'center', marginTop:8}}>
                        <Icon name='dollar-sign' type="feather" containerStyle={{marginRight:16}} color='#757575'/>
                        <View>
                            <Text style={styles.optionTitle}>К оплате</Text>
                            <Text style={styles.optionText}>{(order.price * order.qty + order.deliveryPrice).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")} ₸</Text>
                        </View>
                    </View>

                </View>

                <View style={{...styles.container, padding: 16, flex:1, flexDirection:'column'}}>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Icon name='map-pin' type="feather" containerStyle={{marginRight:16}} color='#757575'/>
                        <View>
                            <Text style={styles.optionTitle}>Адрес доставки</Text>
                            <Text style={styles.optionText}>{order.city}, {order.street} {order.home}</Text>
                        </View>
                    </View>
                </View>
                
            </ScrollView>
        </SafeAreaView>
    )
}

export default OrderDetail

const styles = StyleSheet.create({
    adsImageCover: {
      width: 84,
      height: 84,
      borderRadius: 4,
    },
    container: {
      flex: 1,
      margin: 5,
      backgroundColor: "#fff",
      flexDirection: "row",
      borderRadius: 8,
      marginHorizontal: 16,
      padding: 8
    },
    checkoutContainer: {
      flexDirection: "row",
      justifyContent: 'space-between',
      height: 60,
      alignItems: 'center',
      paddingHorizontal: 8,
      backgroundColor: '#fff',
    },
    optionTitle: {
        fontWeight:'400',
        fontSize: 13,
        color: '#9E9E9E'
    },
    optionText: {
        fontWeight:'400',
        fontSize: 15,
        color: '#212121',
        lineHeight: 17.9,
        letterSpacing: -0.24
    },
  });
