import React, { useState, useEffect } from 'react';
import * as firebase from 'firebase';
import AuthProfile from '../components/AuthProfile'
import NotAuthProfile from '../components/NotAuthProfile'
import { View, Text, FlatList, TouchableOpacity, ScrollView, Linking } from 'react-native'
import { ListItem, Icon, Header } from 'react-native-elements'
import { SafeAreaView } from 'react-native-safe-area-context';
import { useDispatch, useSelector } from 'react-redux';

const ProfileScreen = ({ navigation }) => {
  const [adsData, setAdsData] = useState([
    {
      'id': 1,
      'name': 'Заказ'
    },
    {
      'id': 2,
      'name': 'Доставка'
    },
    {
      'id': 3,
      'name': 'Оплата заказа'
    },
    {
      'id': 4,
      'name': 'Возврат товара'
    },
    {
      'id': 5,
      'name': 'Юр. лицам'
    },

  ])
  const curUser= useSelector(state => state.user.userReducer.profileData)
  const renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        style={{
          flex: 1,
          marginLeft: index == 0 ? 16 : 0,
          marginRight: 8,
          backgroundColor: '#fff',
          paddingVertical: 9,
          paddingHorizontal: 12,
          borderRadius: 8
        }}
      // onPress={() => navigation.navigate("BookDetail", {
      //     book: item
      // })}
      >
        <Text>{item.name}</Text>
      </TouchableOpacity>
    )
  }
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Header
        centerComponent={{
          text: Object.keys(curUser).length === 0 ? 'Профиль' : curUser.firstName,
          style: { fontWeight: '600', fontSize: 17 },
        }}
        containerStyle={{
          backgroundColor: "#F5F5F5",
          paddingBottom: 12,
        }}
        statusBarProps={{
          backgroundColor: "#F5F5F5",
          barStyle: "dark-content",
        }}
      />
      <ScrollView style={{ flex: 1 }}>

        { !firebase.auth().currentUser?
          <NotAuthProfile navigation={navigation} />:
          <AuthProfile navigation={navigation} curUser={curUser}/>
        }
        
        <TouchableOpacity onPress={() => navigation.navigate('Feedback')}>
        <ListItem containerStyle={{ borderRadius: 4, marginHorizontal: 16, marginVertical: 8 }}>
          <Icon name={"help-circle"} type="feather" />
          <ListItem.Content>
            <ListItem.Title>Связаться с нами</ListItem.Title>
          </ListItem.Content>
          <ListItem.Chevron />
        </ListItem>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={()=>{
            const url='tel://12355654656'
            Linking.openURL(url)
          }}
        >
          <View style={{ backgroundColor: '#fff', marginVertical: 8, padding: 16, borderRadius: 4, marginHorizontal: 16 }}>
            <Text style={{ color: '#4D77FB', fontSize: 17, fontWeight: '400' }}>+ 7 777 777 77 77</Text>
            <Text style={{ color: '#9E9E9E', fontWeight: '500', fontSize: 12 }}>Позвоните, если появились вопросы</Text>
          </View>
        </TouchableOpacity>

        <View style={{ marginVertical: 8 }}>
          <FlatList
            data={adsData}
            renderItem={renderItem}
            keyExtractor={item => `${item.id}`}
            horizontal
            showsHorizontalScrollIndicator={false}
          />
        </View>

      </ScrollView>
    </SafeAreaView>
  );
}


export default ProfileScreen;