import React, { useState,useEffect } from 'react';
import { View, Text, TextInput, StyleSheet, Alert, SafeAreaView, ScrollView,TouchableOpacity } from 'react-native';
//import { loggingOut } from '../API/firebaseMethods';
import {
  Header,
  Button,
  ListItem,
  Icon
} from "react-native-elements";
import { useDispatch, useSelector } from 'react-redux';
import { changeProfileData, loggingOut } from '../actions/userActions';


const PersonalDataScreen = (props) => {
    const [firstName, setFirstName] = useState()
    const [tel, setTel] = useState()
    const [email, setEmail] = useState()
    const dispatch = useDispatch()
    const profileData = useSelector(state => state.user.userReducer.profileData)

    const handlePress = () => {
        dispatch(loggingOut())
        props.navigation.goBack()
    };

    const handleSubmit = () => {
        dispatch(changeProfileData(firstName, tel, email))
    }

    useEffect(() => {
        setFirstName(profileData.firstName)
        setTel(profileData.tel)
        setEmail(profileData.email)
        return () => {
            setFirstName('')
            setTel('')
            setEmail('')
        }
    }, [])
    return (
        <SafeAreaView style={{flex:1}}>
            <Header
                centerComponent={{
                    text: "Личные данные",
                    style: { fontWeight: "bold", fontSize: 18 },
                }}
                containerStyle={{
                    backgroundColor: "#F5F5F5",
                    paddingBottom: 8,
                }}
                statusBarProps={{
                    backgroundColor: "#F5F5F5",
                    barStyle: "dark-content",
                }}
                rightComponent={
                    <TouchableOpacity
                        onPress={handleSubmit}
                    >
                        <Text style={{color:'#007AFF', fontSize: 17, fontWeight:'500'}}>Готово</Text>
                    </TouchableOpacity>
                }
                leftComponent={
                    <Icon 
                        name='arrow-left' 
                        type="feather" 
                        onPress={() => props.navigation.goBack()} 
                    />
                }
            />
            <ScrollView style={{ padding: 16 }} >

                <Text style={styles.text}>Ваше имя</Text>
                <TextInput
                    style={styles.formInput}
                    placeholder="Введите ваше имя"
                    autoCapitalize="none"
                    value={firstName}
                    onChangeText={(firstName) => setFirstName(firstName) }

                />

                <Text style={styles.text}>Номер телефона</Text>
                <TextInput
                    style={styles.formInput}
                    placeholder="Введите номер телефона"
                    keyboardType = 'number-pad'
                    value={tel}
                    onChangeText={tel => setTel(tel)}
                />

                <Text style={styles.text}>Почта</Text>
                <TextInput
                    style={styles.formInput}
                    placeholder="Введите свою почту"
                    keyboardType = 'number-pad'
                    value={email}
                    onChangeText={email => setEmail(email)}
                />

            </ScrollView>
            <View style={{padding:16}}>

                <TouchableOpacity  style={{marginBottom:16}}
                    onPress={() => {props.navigation.navigate("ChangePassword")}}
                >
                    <ListItem containerStyle={{borderRadius:4, paddingVertical:0}}>
                        <Icon name='lock' type="feather" color='#757575' />
                        <ListItem.Content>
                            <ListItem.Title  style={{paddingVertical:18}}>Изменить пароль</ListItem.Title>
                        </ListItem.Content>
                        <ListItem.Chevron />
                    </ListItem>
                </TouchableOpacity>

                <TouchableOpacity  onPress={handlePress}
                >
                    <ListItem containerStyle={{borderRadius:4, paddingVertical:0}}>
                        <Icon name='log-out' type="feather" color='#F44336' />
                        <ListItem.Content>
                            <ListItem.Title  style={{color:'#F44336', paddingVertical:18}}>Выйти с аккаунта</ListItem.Title>
                        </ListItem.Content>
                    </ListItem>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    )
}

export default PersonalDataScreen

const styles = StyleSheet.create({
    button: {
      paddingVertical: 12,
      borderWidth: 2,
      borderRadius: 8,
      alignSelf: 'center',
    },
    buttonText: {
      fontSize: 17,
      textAlign: 'center',
    },
    formInput: {
      fontSize: 17,
      padding: 14,
      backgroundColor: '#fff',
      marginBottom: 16,
      marginTop: 4,
      borderRadius: 12,
      fontWeight: '500',
      lineHeight: 20.29
    },
    text: {
      color: "#9E9E9E",
      fontSize: 13,
      fontWeight: '400'
    }
  });