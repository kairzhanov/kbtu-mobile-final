import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image, Dimensions } from 'react-native'

const CardVertical = ({item, navigation}) => {
    const numberWithSpaces = x => x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")
    if (item && item !== null)
    return (
        <TouchableOpacity
            style={[{...styles.card}]}
            key={item.id}
            onPress={() => navigation.navigate("BookDetail", {book: item})}
        >
            <Image
                source={{ uri: item.image }}
                resizeMode="cover"
                style={styles.adsImageCover}
            />
                <Text
                    numberOfLines={3} 
                    style={{ fontWeight: '500', fontSize: 12}}
                >
                    {item.name}
                </Text>
                <Text
                    numberOfLines={3} 
                    style={{
                        color:'#9E9E9E', 
                        fontSize: 8
                    }}>Описание определенного товара
                </Text>

                <View 
                    style={{flex:1, flexDirection: 'row', justifyContent:'space-between'}}
                >
                    <Text style={styles.price}>{numberWithSpaces(item.price)} ₸</Text>
                </View>

        </TouchableOpacity>
    )
}

export default CardVertical

const styles = StyleSheet.create({
    header:{
        fontSize: 17,
        color: '#212121',
        marginLeft: 16,
        marginBottom: 6
    },
    card:{
        width: (Dimensions.get('window').width-24-1) * 0.5,
        backgroundColor: '#fff',
        borderRadius: 8,
        padding: 8,
        height:260,
    },
    adsImageCover:{
        width: 160,
        height: 160,
        marginBottom: 8,
        borderRadius: 4
    },
    price: {
        alignSelf:'flex-end',
        fontWeight: '500',
        fontSize: 13,
        width: '100%',
        height: 16
    }
})