import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'

const CardHorizontal = ({item, navigation}) => {
    const numberWithSpaces = x => x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")
    return (
        <TouchableOpacity
            style={styles.productContainer}
            onPress={() => navigation.navigate("BookDetail", {
                book: item
            })}
        >
            <Image
                source={{ uri: item.image }}
                resizeMode="cover"
                style={styles.adsImageCover}
            />
            <View style={styles.productInfoContainer}>
                <View>
                    <Text numberOfLines={2} style={styles.productTitle}>{item.name}</Text>
                    <Text style={styles.productDesciption}>Описание определенного товара</Text>
                </View>
                <Text>{numberWithSpaces(item.price)} ₸</Text>
            </View>
        </TouchableOpacity>
    )
}

export default CardHorizontal

const styles = StyleSheet.create({
    droidSafeArea: {
        flex: 1,
        paddingTop: Platform.OS === "android" ? 25 : 0,
        backgroundColor: "#F5F5F5",
    },
    adsImageCover: {
        width: 84,
        height: 84,
        borderRadius: 4,
        marginRight: 8
    },
    productContainer: {
        flex: 1,
        padding: 8,
        backgroundColor: "#fff",
        flexDirection: "row",
        borderRadius: 8,
        height: 100,
        marginVertical: 4,
        marginHorizontal: 8
    },
    productInfoContainer: { 
        flex: 1, 
        justifyContent: "space-between" 
    },
    productDesciption: { 
        color: "#9E9E9E", 
        fontSize: 10, 
        fontWeight: '500' 
    },
    productTitle: { 
        fontWeight: '500', 
        fontSize: 13 
    }
});
