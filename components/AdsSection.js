import React, {useState} from "react";
import {
    TouchableOpacity,
    Image,
    FlatList,
    StyleSheet
} from 'react-native';


const AdsSection = (props) => {

    const [adsData, setAdsData] = useState(props.adsList)
    const renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity style={[styles.imageContainer, {marginLeft: index == 0 ? 16 : 0}]}>
                <Image
                    source={item.bookCover}
                    resizeMode="cover"
                    style={styles.adsImageCover}
                />
            </TouchableOpacity>
        )
    }
    return (
        <FlatList
            data={adsData}
            renderItem={renderItem}
            keyExtractor={item => `${item.id}`}
            horizontal
            showsHorizontalScrollIndicator={false}
        />
    )
}
const styles = StyleSheet.create({
    adsImageCover:{
        width: 120,
        height: 160,
        borderRadius: 8
    },
    imageContainer: {
        flex: 1,
        marginRight: 8,
        marginTop: 12
    }
})

export default AdsSection