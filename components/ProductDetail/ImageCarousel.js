import React, { useEffect, useRef, useState } from 'react'
import { StyleSheet, Text, View, Image, Dimensions } from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel';

const screenWidth  = Dimensions.get('window').width;

const ImageCarousel = ({images}) => {
    
    const carouselRef = useRef(null);
    const [activeIndex, setActiveIndex] = useState(0);
    const [prodImages, setProdImages] = useState([])
    useEffect(() => {
        let notNullImages = images.filter(image => image !== null)
        setProdImages(notNullImages)
        return () => {
            setProdImages()
        }
    }, [])
    const _renderItem = ({item}) => {
        return (
            <Image
                source={{ uri: item }}
                resizeMode="cover"
                style={{
                    width: '100%',
                    height: 343
                }}
            />
        );
    }

    return (
        <View style={styles.container}>
            <Carousel
                ref={carouselRef}
                data={prodImages && Object.keys(prodImages).map(key => prodImages[key])}
                renderItem={_renderItem}
                sliderWidth={screenWidth}
                sliderHeight={screenWidth}
                itemWidth={screenWidth - 32}
                inactiveSlideOpacity={0}
                inactiveSlideScale={1}
                onSnapToItem={(index) => setActiveIndex(index)}
            />
            <Pagination
                dotsLength={prodImages && Object.keys(prodImages).map(key => prodImages[key]).length}
                activeDotIndex={activeIndex}
                containerStyle={styles.pagination}
                dotStyle={styles.activeDotStyle}
                inactiveDotStyle={styles.inactiveDotStyle}
                inactiveDotOpacity={1}
                inactiveDotScale={1}
            />
        </View>
    )
}

export default ImageCarousel

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        flexDirection: 'row',
        marginBottom: 8
    },
    pagination: { 
        backgroundColor: 'rgba(0, 0, 0, 0)', 
        position:'absolute', 
        bottom:-14
    },
    activeDotStyle: {
        borderRadius: 6,
        backgroundColor: '#F44336',
        width:6,
        height:6
    },
    inactiveDotStyle:{
        borderRadius: 6,
        backgroundColor: '#757575',
    }
})
