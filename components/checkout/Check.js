import React, { useState } from 'react'
import { StyleSheet, Text, View, ScrollView, TextInput } from 'react-native'
import { useSelector } from 'react-redux'

const Check = (props) => {
    return (
            <ScrollView>

                <View style={{paddingHorizontal:16, paddingTop:8}}>
                    <Text style={styles.text}>Получатель</Text>
                    <Text style={styles.output}>{props.firstName}</Text>

                    <Text style={styles.text}>Адрес доставки</Text>
                    <Text style={styles.output}>г. {props.city}, {props.street} {props.home}, кв. {props.flat}</Text>

                    <Text style={styles.text}>Дата доставки</Text>
                    <Text style={styles.output}>{props.deliveryDate}</Text>

                    <Text style={styles.text}>Оплата</Text>
                    <Text style={styles.output}>{props.payMethod.title}</Text>
                </View>

                <View style={{backgroundColor:'#EEEEEE', paddingHorizontal:16, paddingVertical:8}}>
                    <View style={{flex:1, flexDirection:'row', justifyContent:'space-between', marginVertical:8}}>
                        <Text style={{fontSize:15}}>Товары</Text>
                        <Text style={{fontSize:15, fontWeight:'600'}}>{props.order.cartPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")} ₸</Text>
                    </View>

                    <View style={{flex:1, flexDirection:'row', justifyContent:'space-between', marginVertical:8}}>
                        <Text style={{fontSize:15}}>Доставка</Text>
                        <Text style={{fontSize:15, fontWeight:'600'}}>{(1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")} ₸</Text>

                    </View>
                    <View style={{flex:1, flexDirection:'row', justifyContent:'space-between', marginVertical:8}}>
                        <Text style={{fontSize:15}}>Всего</Text>
                        <Text style={{fontSize:15, fontWeight:'600'}}>{(props.order.cartPrice+1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")} ₸</Text>
                    </View>
                </View>
                <TextInput
                    mode="outlined"
                    multiline
                    style={styles.formInput}
                    placeholder="Комментарии к заказу"
                    onChangeText={(comment) => props.setComment(comment)}
                    value={props.comment}
                >

                </TextInput>
            </ScrollView>
    )
}

export default Check
const styles = StyleSheet.create({
    button: {
      paddingVertical: 12,
      borderWidth: 2,
      borderRadius: 8,
      alignSelf: 'center',
    },
    buttonText: {
      fontSize: 17,
      textAlign: 'center',
    },
    output: {
      fontSize: 17,
      marginBottom: 16,
      marginTop: 4,
    },
    text: {
      color: "#9E9E9E",
    },
    formInput:{
        paddingHorizontal: 16,
        paddingVertical:14,
        borderRadius:8,
        backgroundColor: '#fff',
        fontSize: 17,
        fontWeight: '500',
        marginHorizontal:16,
        marginVertical:8,
        minHeight:80
    }
  });
