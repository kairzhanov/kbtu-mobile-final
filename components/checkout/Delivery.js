import React, {useState} from 'react'
import { View, ScrollView, Text, StyleSheet, TextInput, SafeAreaView } from 'react-native'
import { Header, Icon, Button } from 'react-native-elements'
import moment from "moment";


const Delivery = (props) => {
    return (
        <ScrollView style={{padding:16, flex:1}}>

            <Text style={styles.text}>Город</Text>
            <TextInput
                style={styles.formInput}
                placeholder="Укажите город"
                onChangeText={(city) => props.setCity(city)}
                value={props.city}
            />

            <Text style={styles.text}>Улица</Text>
            <TextInput
                onChangeText={(street) => props.setStreet(street)}
                style={styles.formInput}
                placeholder="Укажите улицу"
                value={props.street}
            />

            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: -8 }}>
                <View style={{ flex: 1, marginHorizontal: 8 }}>
                    <Text style={styles.text}>Дом</Text>
                    <TextInput
                        style={styles.formInput}
                        placeholder="Номер дома"
                        onChangeText={(home) => props.setHome(home)}
                        value={props.home}
                    />
                </View>
                <View style={{ flex: 1, marginHorizontal: 8 }}>
                    <Text style={styles.text}>Квартира</Text>
                    <TextInput
                        style={styles.formInput}
                        placeholder="Номер квартиры"
                        onChangeText={(flat) => props.setFlat(flat)}
                        value={props.flat}
                    />
                </View>
            </View>

            <View style={{ marginBottom: 10, flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                <Icon name={"calendar"} type="feather" color='#9E9E9E' />
                <View style={{ marginLeft: 16 }} >
                    <Text>{"Дата доставки"}</Text>
                    <Text>{moment(props.deliveryDate).format('LL')}</Text>
                </View>
            </View>

            <View style={{ marginBottom: 10, flex:1, flexDirection:'row', alignItems:'center'  }}>
                    <Icon name={"truck"} type="feather" color='#9E9E9E'/>
                    <View style={{marginLeft:16}} >
                    <Text>{"Стоимость доставки"}</Text>
                    <Text>{props.deliveryPrice} ₸</Text>
                    </View>
            </View>

        </ScrollView>
    )
}

export default Delivery



const styles = StyleSheet.create({
    button: {
      paddingVertical: 12,
      borderWidth: 2,
      borderRadius: 8,
      alignSelf: 'center',
    },
    buttonText: {
      fontSize: 17,
      textAlign: 'center',
    },
    formInput: {
      fontSize: 17,
      paddingVertical: 14,
      paddingHorizontal:16,
      backgroundColor: '#fff',
      marginBottom: 16,
      marginTop: 4,
      borderRadius: 8,
      fontWeight:'500'
    },
    text: {
      color: "#9E9E9E",
    },
    operText:{
        fontSize:10,
        fontWeight:'500',
        lineHeight: 12,
        textAlign:'center',
        marginTop: 4
    },
    separator: {
        height: 2, 
        backgroundColor: 'black',
        marginHorizontal:4,
        marginTop:12,
        borderRadius:10,
        width: 38
    }
  });
