import React, { useEffect } from 'react'
import { StyleSheet, Text, View,TouchableOpacity, CheckBox } from 'react-native'
import { Icon, ListItem } from 'react-native-elements'
import { FlatList } from 'react-native-gesture-handler'

const Payment = (props) => {

    let pm = [
        {name:'CARD', title:'Безналичный расчет'}, 
        {name:'CASH', title:'Наличными'}, 
        {name:'POD', title:'Наложенный платеж'}, 
    ]

    const [pay, setPay] = React.useState({name:'CASH', title:'Наличными'})
    useEffect(() => {
        props.setPayMethod(pay)
    }, [pay])

    const renderItem =({item}) => {
        return (
            <TouchableOpacity
                onPress={() => {
                    setPay(item)
                    props.setPayMethod(pay)
                }}

            >
                <ListItem containerStyle={{ borderRadius: 4, marginBottom:8, padding: 16}}>
                    <Icon name={"credit-card"} type="feather" />
                    <ListItem.Content>
                        <ListItem.Title>{item.title}</ListItem.Title>
                    </ListItem.Content>
                    <View style={pay.name === item.name ? styles.checkedContainer : styles.notCheckedContainer}>
                        <View style={pay.name === item.name ? styles.checkedBox :styles.notCheckedBox}></View>
                    </View>
                </ListItem>
            </TouchableOpacity>
        )
    }

    return (
        <FlatList
            data={pm}
            renderItem={item => renderItem(item)}
            showsVerticalScrollIndicator={false}
            keyExtractor={(item) => item.name.toString()}
            contentContainerStyle={{paddingHorizontal:16, paddingTop:8}}
        />
    )
}

export default Payment

const styles = StyleSheet.create({
    checkedContainer:{
        padding:2,
        borderWidth:1, 
        borderColor:'red',
        borderRadius:3, 
        alignItems:'center', 
        justifyContent:'center'
    },
    checkedBox:{
        backgroundColor:'red',
        width:14,
        height:14,
        borderRadius:1
    },
    notCheckedContainer:{
        padding:1,
        borderWidth:1,
        borderColor:'#9E9E9E',
        borderRadius:3
    },
    notCheckedBox:{
        backgroundColor:'#fff', 
        width:14, 
        height:14, 
        borderRadius:1
    }
})
