import React from 'react'
import { StyleSheet, Text, View, TextInput } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'

const Data = (props) => {
    return (
        <ScrollView style={{padding:16}}>
            <Text style={styles.text}>Ваше имя</Text>
            <TextInput
                style={styles.formInput}
                placeholder="Введите Ваше имя"
                onChangeText={(firstName) => props.setFirstName(firstName)} 
                value={props.firstName}
            />
            <Text style={styles.text}>Номер телефона</Text>
            <TextInput
                style={styles.formInput}
                placeholder="+7 ( --- ) --- -- --"
                onChangeText={(tel) => props.setTel(tel)} 
                value={props.tel}
            />
            <Text style={styles.text}>Почта</Text>
            <TextInput
                style={styles.formInput}
                placeholder="Введите ваш e-mail"
                onChangeText={(mail) => props.setEmail(mail)} 
                value={props.email}
            />
        </ScrollView>
    )
}

export default Data

const styles = StyleSheet.create({
    formInput: {
      fontSize: 17,
      paddingVertical: 14,
      paddingHorizontal:16,
      backgroundColor: '#fff',
      marginBottom: 16,
      marginTop: 4,
      borderRadius: 8,
      fontWeight:'500'
    },
    text: {
      color: "#9E9E9E",
    },
  });
