import React, { useState } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import { ListItem, Icon, BottomSheet } from 'react-native-elements'
import { useSelector } from "react-redux";


function AuthProfile(props) {
    const [isVisible, setIsVisible] = useState(false);
    const [currency, setCurrency] = useState('KZT')
    const city = useSelector(state => state.cart.cartReducer.city)
    const list = [
        { title: "Тенге (KZT)", code: 'KZT' },
        { title: "Рубль (RUB)", code: 'RUB'},
        { title: "Доллар (USD)", code: 'USD'}
    ];
    return (
        <View style={{ paddingHorizontal: 16, paddingVertical: 8 }}>

            <TouchableOpacity
                style={{ marginVertical: 16 }}
                onPress={() => {
                    props.navigation.navigate("Orders")
                }}
            >
                <ListItem
                    containerStyle={{
                        borderRadius: 4
                    }}
                >
                    <Icon name={"box"} type="feather" />
                    <ListItem.Content>
                        <ListItem.Title>Мои заказы</ListItem.Title>
                    </ListItem.Content>
                    <ListItem.Chevron />
                </ListItem>
            </TouchableOpacity>

            <View>

                <ListItem
                    onPress={() => setIsVisible(true)}
                    bottomDivider
                >
                    <Icon name={"dollar-sign"} type="feather" />
                    <ListItem.Content>
                            <ListItem.Title>Валюта {currency}</ListItem.Title>
                    </ListItem.Content>
                    <ListItem.Chevron />
                </ListItem>

                <ListItem bottomDivider 
                        onPress={() => props.navigation.navigate('SelectCity')}>
                    <Icon name={"map-pin"} type="feather" />
                    <ListItem.Content>
                        <ListItem.Title>{city.city}</ListItem.Title>
                    </ListItem.Content>
                    <ListItem.Chevron />
                </ListItem>

                <TouchableOpacity onPress={() => props.navigation.navigate("PersonalData", { curUser: props.curUser })}
                >
                    <ListItem>
                        <Icon name={"edit-3"} type="feather" />
                        <ListItem.Content>
                            <ListItem.Title>{"Личные данные"}</ListItem.Title>
                        </ListItem.Content>
                        <ListItem.Chevron />
                    </ListItem>
                </TouchableOpacity>


            </View>

            <BottomSheet
                isVisible={isVisible}
                containerStyle={{  padding: 8 }}
                modalProps={{ animationType: 'fade' }}

            >
                {
                    list.map((l, i) => (
                        <ListItem
                            key={i}
                            containerStyle={[
                                i===0 && {borderTopLeftRadius:8, borderTopRightRadius:8}, 
                                i===list.length-1 && {borderBottomLeftRadius:8, borderBottomRightRadius:8},
                                {paddingVertical:18}]}
                            onPress={()=>{
                                l.code!==currency && setCurrency(l.code)
                                l.code!==currency && setIsVisible(false)
                            }}
                            bottomDivider
                        >

                            <Icon name={'check'} color={l.code!==currency  && '#fff'} type="feather"/>
                            <ListItem.Content>
                                <ListItem.Title style={{...styles.currencyText, color: l.code ===currency ? '#BDBDBD' : '#212121'}}>{l.title}</ListItem.Title>
                            </ListItem.Content>

                        </ListItem>
                ))}
                <TouchableOpacity style={{
                    paddingVertical: 16,
                    alignItems: 'center',
                    backgroundColor: '#fff',
                    marginVertical: 8,
                    borderRadius: 8

                }} 
                onPress ={() => setIsVisible(false)} >
                    <Text style={{
                        color: '#4D77FB',
                        fontSize: 20,
                        fontWeight: '600'
                    }} >Отмена</Text>
                </TouchableOpacity>
            </BottomSheet>
      
        </View>

    )
}

const styles = StyleSheet.create({
    btnContainer: {
        marginVertical: 10,
        backgroundColor: '#4D77FB',
        paddingVertical: 12,
        borderRadius: 8,

    },
    buttonText: {
        color: '#fff',
        textAlign: 'center',
        fontSize: 17
    },
    currencyText: {
        fontWeight:'400',
        fontSize: 17
    }
})

export default AuthProfile