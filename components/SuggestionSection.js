import React, { useState } from "react";
import { StyleSheet, Text, View, Dimensions } from "react-native";

const SuggestionSection = () => {
  const [griditem, setGriditem] = useState([
    { key: "Вам понравится", color: '#FA9D00' },
    { key: "Вам не понравится", color: '#B4E9FD' },
    { key: "Вам должно понравится", color: '#B79BFC' },
    { key: "Вам не должно понравится", color: '#C5E1A5' }
  ]);
  return (
    <View style={{ flexWrap: 'wrap', flexDirection: 'row', marginHorizontal: 12, }} >
      {
        griditem.map((item, index) => (
          <View key={index} style={[styles.GridViewBlockStyle, { backgroundColor: item.color }]}>
            <Text style={styles.GridViewInsideTextItemStyle}> {item.key} </Text>
          </View>
        ))
      }
    </View>
  );
};

const styles = StyleSheet.create({

  GridViewBlockStyle: {
    width: (Dimensions.get('window').width - 40 - 1) * 0.5,
    height: (Dimensions.get('window').width - 40 - 1) * 0.5,
    margin: 4,
    padding: 12,
    justifyContent: 'flex-end',
    borderRadius: 8

  },
  GridViewInsideTextItemStyle: {
    color: "#212121",
    fontSize: 12,
    justifyContent: "center",
    fontWeight: '600',
    textAlign: 'center'
  },
});

export default SuggestionSection;
