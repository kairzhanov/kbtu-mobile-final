import React, {useState} from "react";
import {
    View,
    TouchableOpacity,
    Image,
    FlatList,
    StyleSheet,
    Text
} from 'react-native';
import { SIZES } from '../constants';


const CardCarousel = (props) => {

    const [adsData, setAdsData] = useState(props.adsList)
    const {navigation, title} = props

    const renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity
                style={[{...styles.card},{marginLeft: index == 0 ? 16 : 0}]}
                onPress={() => navigation.navigate("BookDetail", {book: item})}
            >
                <Image
                    source={item.bookCover}
                    resizeMode="cover"
                    style={styles.adsImageCover}
                />
                    <Text
                        numberOfLines={3} 
                        style={{ fontWeight: '500', fontSize: 12}}
                    >
                        {item.bookName}
                    </Text>
                    <Text
                        numberOfLines={3} 
                        style={{
                            color:'#9E9E9E', 
                            fontSize: 8
                        }}>{item.description}
                    </Text>

                    <View 
                        style={{flex:1, flexDirection: 'row', justifyContent:'space-between'}}
                    >
                        <Text style={styles.price} >777 777 $</Text>
                        {/* <Text>4.3</Text> */}
                    </View>

            </TouchableOpacity>
        )
    }
    return (
        <View style={{ flex: 1, marginVertical: 9 }}>
            <Text style={styles.header}>{title}</Text>
            <FlatList
                data={adsData}
                renderItem={renderItem}
                keyExtractor={item => `${item.id}`}
                horizontal
                showsHorizontalScrollIndicator={false}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    header:{
        fontSize: 17,
        color: '#212121',
        marginLeft: 16,
        marginBottom: 6
    },
    card:{
        marginRight: SIZES.radius,
        width: 150,
        backgroundColor: '#fff',
        borderRadius: 8,
        padding: 8,
        height:234
    },
    adsImageCover:{
        width: 134,
        height: 134,
        marginBottom: 8,
        borderRadius: 4
    },
    price: {
        alignSelf:'flex-end',
        fontWeight: '500',
        fontSize: 13,
        width: '100%',
        height: 16
    }
})

export default CardCarousel