import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Icon } from 'react-native-elements'
import moment from 'moment'

const Canceled = (props) => {
    return (
        <View style={{...styles.container, padding:16, flexDirection:'column'}}>

            <Text style={{...styles.status, marginBottom:20}}>Отменен</Text>

            <View style={{flexDirection:'row', alignItems:'center', marginVertical: 4}}>
                <Icon name='check-circle' type="feather" containerStyle={{marginRight:16}} color='#66BB6A'/>
                <View>
                    <Text style={{fontWeight:'500', fontSize:15, lineHeight:17.9, letterSpacing: -0.24, color:'#66BB6A'}}>Заявка принята</Text>
                    <Text style={{fontWeight:'500', fontSize:12, lineHeight:14.32, color:'#9E9E9E'}}>{moment(props.orderDay).subtract(7,'d').format('LL')}</Text>
                </View>
            </View>

            <View style={{flexDirection:'row', alignItems:'center', marginVertical: 4}}>
                <Icon name='more-vertical' type="feather" containerStyle={{marginRight:16}} color='#757575'/>
            </View>

            <View style={{flexDirection:'row', alignItems:'center', marginVertical: 4}}>
                <Icon name='x-circle' type="feather" containerStyle={{marginRight:16}} color='#757575'/>
                <View>
                    <Text style={{fontWeight:'500', fontSize:15, lineHeight:17.9, letterSpacing: -0.24, color:'#212121'}}>Отменен</Text>
                    <Text style={{fontWeight:'500', fontSize:12, lineHeight:14.32, color:'#9E9E9E'}}>{moment(props.orderDay).subtract(6,'d').format('LL')}</Text>
                </View>
            </View>
        </View>

    )
}

export default Canceled

const styles = StyleSheet.create({    
    container: {
        flex: 1,
        margin: 5,
        backgroundColor: "#fff",
        flexDirection: "row",
        borderRadius: 8,
        marginHorizontal: 16,
        padding: 8
    },
    status: {
        color:'#212121',
        fontWeight:'600',
        fontSize: 27,
        lineHeight: 32.22,
        letterSpacing: -0.24
  }
})
