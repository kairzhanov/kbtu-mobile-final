import React, { useState, useEffect, useCallback } from "react";
import {
  View,
  TouchableOpacity,
  FlatList,
  StyleSheet,
  Text,
} from "react-native";
import { SIZES, COLORS } from "../constants";
import { Icon } from "react-native-elements";
import { useHttp } from '../hooks/http.hook'
import { LinearProgress } from 'react-native-elements';

const CategoryHeaderSection = (props) => {
  const [categoriesData, setCategoriesData] = useState();
  const { loading, request } = useHttp()

  const fetchCats = useCallback(async () => {
    try {
      const fetched = await request('https://doxjo-10749.firebaseio.com/categories.json', 'GET', null)
      setCategoriesData(fetched.slice(1))
    } catch (e) { }
  },
    [request])
  useEffect(() => {
    fetchCats()
  }, [])
  const renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        style={{ marginRight: 15, width: 65, alignItems: 'center',marginLeft: index == 0 ? 16 : 0 }}
        onPress={() => {
          props.navigation.navigate("ProductSection", {
            products: item
          })
        }}
      >
        <Icon
          name={item.icon}
          type={item.iconType}
          containerStyle={{
            backgroundColor: "#F9365E",
            paddingTop: 19,
            borderRadius: 200,
            height: 56,
            width: 56,
            marginBottom: 4
          }}
          color="#fff"
          size={18}
        />
        <Text
          style={{
            color: COLORS.black,
            flexWrap: "wrap",
            fontSize: 10,
            textAlign: 'center',
            fontWeight: '500'
          }}
        >
          {item.name}
        </Text>
      </TouchableOpacity>
    );
  };

  if (loading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginVertical: 20, padding: 16 }}>
        <Text>Загрузка...</Text>
        <LinearProgress
          color={'secondary'}
          style={{ marginHorizontal: 16 }}
        />
      </View>
    )
  }
  return (
    <View style={styles.сategoryHeaderContainer}>
      {
        !loading && (
          <FlatList
            data={categoriesData}
            showsHorizontalScrollIndicator={false}
            renderItem={renderItem}
            keyExtractor={item => `${item.id}`}
            horizontal
          />
        )
      }
    </View>
  );
};

const styles = StyleSheet.create({
  сategoryHeaderContainer: {
    marginVertical: 16
  },
});

export default CategoryHeaderSection;