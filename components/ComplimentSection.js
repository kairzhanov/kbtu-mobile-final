import React, { useState } from "react";
import { StyleSheet, Text, View, Dimensions } from "react-native";

const ComplimentSection = () => {
  const [griditem, setGriditem] = useState([
    { key: "Ты лучший", color: '#1B5E20' },
    { key: "А может и нет", color: '#0062A5' },
    { key: "А може может", color: '#8800E3' },
    { key: "А може может", color: '#0027BC' }
  ]);
  return (
    <View style={{flexWrap:'wrap', flexDirection:'row', marginHorizontal:12, }} >
      {
        griditem.map((item, index) => (
          <View key={index} style={[styles.GridViewBlockStyle, {backgroundColor: item.color}]}>
            <Text style={styles.GridViewInsideTextItemStyle}> {item.key} </Text>
          </View>
        ))
      }
      </View>
  );
};

const styles = StyleSheet.create({

  GridViewBlockStyle: {
    width: (Dimensions.get('window').width-40-1) * 0.5,
    height: (Dimensions.get('window').width-40-1) * 0.5,
    margin:4,
    padding:12,
    justifyContent:'flex-end',
    borderRadius:8
    
  },
  GridViewInsideTextItemStyle: {
    color: "#fff",
    fontSize: 12,
    justifyContent: "center",
    fontWeight:'600',
    textAlign: 'center'
  },
});

export default ComplimentSection;
