import React, { useState, useEffect } from "react";
import { StyleSheet, View, Text, TouchableOpacity, ScrollView, FlatList } from "react-native";
import { ListItem, Icon, BottomSheet } from 'react-native-elements'
import { SafeAreaView } from "react-native-safe-area-context";
import { useDispatch, useSelector } from "react-redux";
import { changeCurrency } from "../actions/CartActions";


function NotAuthProfile({ navigation }) {
    const [isVisible, setIsVisible] = useState(false);
    const currency = useSelector(state => state.cart.cartReducer.currency)
    const dispatch = useDispatch()
    const list = [
        { title: "Тенге (KZT)", isSelected: true, code: 'KZT' },
        { title: "Рубль (RUB)", isSelected: false, code: 'RUS'},
        { title: "Доллар (USD)", isSelected: false, code: 'USD'}
    ];
    return (
        <SafeAreaView>
            <ScrollView style={{ paddingHorizontal: 16 }} >

                <TouchableOpacity
                    onPress={() => setIsVisible(true)}>
                    <ListItem
                        containerStyle={{ borderTopLeftRadius: 4, borderTopRightRadius: 4 }}
                        bottomDivider
                    >
                        <Icon name={"dollar-sign"} type="feather" />
                        <ListItem.Content>
                            <ListItem.Title>{`Валюта (${currency})`}</ListItem.Title>
                        </ListItem.Content>
                        <ListItem.Chevron />
                    </ListItem>
                </TouchableOpacity>

                <TouchableOpacity
                        onPress={() => navigation.navigate('SelectCity')}
                >
                    <ListItem
                        containerStyle={{
                            borderBottomLeftRadius: 4,
                            borderBottomRightRadius: 4
                        }}
                    >
                        <Icon name={"map-pin"} type="feather" />
                        <ListItem.Content>
                            <ListItem.Title>{"Алматы"}</ListItem.Title>
                        </ListItem.Content>
                        <ListItem.Chevron />
                    </ListItem>
                </TouchableOpacity>

                <View style={styles.SignUpOrIn} >

                    <Text style={{ fontSize: 20, fontWeight: '600' }} >Вы еще не авторизовались</Text>
                    <Text style={styles.SignUpOrInContent}>
                        Войдите в свой аккаунт, чтобы
                        получить доступ к информации
                        о заказах и многому другому!
                    </Text>

                    <TouchableOpacity
                        style={styles.btnContainer}
                        onPress={() => navigation.navigate('Sign In')}
                    >
                        <Text style={styles.buttonText}>Войти</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={{
                            marginVertical: 16
                        }}
                        onPress={() => navigation.navigate('Sign Up')}
                    >
                        <Text
                            style={{
                                color: '#4D77FB',
                                fontSize: 17,
                                fontWeight: '500'
                            }}
                        >
                            Создать аккаунт
                        </Text>
                    </TouchableOpacity>
                </View>


                <BottomSheet
                isVisible={isVisible}
                containerStyle={{  padding: 8 }}
                modalProps={{ animationType: 'fade' }}

            >
                {
                    list.map((l, i) => (
                        <ListItem
                            key={i}
                            containerStyle={[
                                i===0 && {borderTopLeftRadius:8, borderTopRightRadius:8}, 
                                i===list.length-1 && {borderBottomLeftRadius:8, borderBottomRightRadius:8},
                                {paddingVertical:18}]}
                            onPress={()=>{
                                dispatch(changeCurrency(l.code))
                                setIsVisible(false)
                            }}
                            bottomDivider
                        >

                            <Icon name={'check'} color={l.code!==currency  && '#fff'} type="feather"/>
                            <ListItem.Content>
                                <ListItem.Title style={styles.currencyText}>{l.title}</ListItem.Title>
                            </ListItem.Content>

                        </ListItem>
                ))}
                <TouchableOpacity style={{
                    paddingVertical: 16,
                    alignItems: 'center',
                    backgroundColor: '#fff',
                    marginVertical: 8,
                    borderRadius: 8

                }} 
                onPress ={() => setIsVisible(false)} >
                    <Text style={{
                        color: '#4D77FB',
                        fontSize: 20,
                        fontWeight: '600'
                    }} >Отмена</Text>
                </TouchableOpacity>
            </BottomSheet>
      
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    btnContainer: {
        paddingVertical: 12,
        backgroundColor: '#4D77FB',
        borderRadius: 8,
        alignItems: 'center',
        width: '100%'
    },
    buttonText: {
        color: '#fff',
        fontSize: 17,
        fontWeight: '500'
    },
    SignUpOrIn: {
        backgroundColor: '#fff',
        marginVertical: 16,
        paddingVertical: 16,
        paddingHorizontal: 40,
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        borderRadius: 4
    },
    SignUpOrInContent: {
        fontSize: 17,
        fontWeight: '400',
        letterSpacing: 0.5,
        lineHeight: 20.29,
        marginBottom: 40,
        textAlign: 'center',
        marginTop: 8
    },
    buttonText: {
        color: '#fff',
        textAlign: 'center',
        fontSize: 17
    },
    currencyText: {
        fontWeight:'400',
        fontSize: 17
    }
})

export default NotAuthProfile
